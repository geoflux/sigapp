import alias from '@rollup/plugin-alias'
import svelte from 'rollup-plugin-svelte'
import resolve from 'rollup-plugin-node-resolve'
import replace from '@rollup/plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import livereload from 'rollup-plugin-livereload'
import { terser } from 'rollup-plugin-terser'
import { config } from 'dotenv'

config()
const production = !process.env.ROLLUP_WATCH

export default {
	input: 'src/main.js',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		file: 'public/app/bundle.js'
	},
	plugins: [
		alias({
			entries: { 
				app: 'src/modules', 
				auth: 'src/auth' 
			}
		}),
		replace({
			API_URL: process.env.API_URL,
			AUTH_API_URL: process.env.AUTH_API_URL,
			QGIS_SERVER_URL: process.env.QGIS_SERVER_URL,
			AUTH_URL: process.env.AUTH_URL,
			GUESTS: process.env.GUESTS,
			RESOURCES_URL: process.env.RESOURCES_URL,
			GEOCODER_API_URL: process.env.GEOCODER_API_URL,
			GEOCODER_FILTER: process.env.GEOCODER_FILTER,
		}),
		svelte({
			dev: !production,
			css: css => {
				css.write('public/app/bundle.css')
			}
		}),
		resolve({
			browser: true,
			dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/')
		}),
		commonjs(),
		!production && livereload('public'),
		production && terser()
	],
	watch: {
		clearScreen: false
	},
	onwarn: function(warning, superOnWarn) {
		if (warning.code === 'THIS_IS_UNDEFINED') {
		  return
		}
		superOnWarn(warning)
	}
}
