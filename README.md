<div align="center">
	<img align="center" src="public/app/img/default-logo.svg" alt="drawing" width="172"/>
	<p align="center">
		<h1 align="center" style="font-size:32px;margin:0;border:none;">Sigapp</h1>
		<img src="https://img.shields.io/badge/PHP->=7.2-yellow.svg?logo=PHP&color=777bb4&labelColor=455a64" alt="PHP"/>
		<img src="https://img.shields.io/badge/QGIS_Server->=3.4-yellow.svg?logo=Qgis&color=93b023&labelColor=455a64" alt="Qgis"/>
		<img src="https://img.shields.io/badge/Svelte-3-yellow.svg?logo=Svelte&color=ff3e00&labelColor=455a64" alt="Svelte"/>
		<img src="https://img.shields.io/badge/Openlayers-6.1.1-yellow.svg?logo=Openlayers&color=1f6b75&labelColor=455a64" alt="Qgis"/><br>
		<img src="https://img.shields.io/badge/Version-0.0.1-blue.svg?color=0288d1&labelColor=455a64" alt="Version"/>
		<img src="https://img.shields.io/badge/License-MIT-blue.svg?color=0288d1&labelColor=455a64" alt="License"/>
	</p>
</div>



# <a name="abstract"></a>Résumé
Sigapp est un WebSIG propulsé par QGIS Server comprenant une API REST et un client web. A l’appuie du serveur cartographique et de son moteur de rendu, Sigapp inclut :

- Un client de webmapping
- La fonctionnalité d'import de projet QGIS
- Un module d'administration SIG



|   |   |
|---|---|
| Démo  | https://geoflux.io/sigapp  |
|  API | https://geoflux.io/sigapp/docs  |

## Webmapping
Le client WEB Sigapp a pour objectif de faciliter l'accès au SIG pour tout profil utilisateur. Un portail regroupe des cartes pré-définies. <br>
Il est possible d'interroger les données géographiques (table attributaire, interrogation d'entité...), de filtrer les données, de consulter l'ensemble du SIG, d'ajouter / supprimer une couche, ou encore de composer une carte et d'enregistrer ses modifications. <br>
Chaque couche est indépendante du projet QGIS d'origine. Les données sources sont exportables aux formats Excel, GeoJSON et Shapefile. <br>
Le client cartographique permet d'imprimer une carte : export PNG ou PDF avec légende + titre + logo modulables, aux formats standards A4...Ax.

## Imports de projets QGIS
Sigapp inclut un analyseur/parser QGIS qui permet notamment de reconstituer un SIG à partir de fichiers projets QGS/QGZ.

### Chargement basique (fichiers QGS ou QGZ)
Sigapp sauvegarde les propriétés des couches et les connexions aux bases de données enregistrées dans les fichiers QGIS*. Chaque couhe du fichier de projet devient un webmap-service (WMS) indépendant. <br>
Pour optimiser l'indexation dans le catalogue SIG, des informations peuvent être renseignées depuis QGIS Desktop : méta données, alias de champs, nom des couches… Le module d'administration permet également de définir ou mettre à jour ces valeurs.<br>
<i style="font-size:12px;">* Le processus de récupération des connexions aux SGBD n'est pas sécurisé actuellement : fonctionnalité en cours de développement.</i>

### Embarquer des données locales (fichier ZIP)
Sigapp inclut un datastore Postgis destiné à injecter les sources de données fichier associées à un projet QGIS (Spatialite, Shapefiles, GeoJSON, Geopackage). Le zip doit contenir le projet accompagné des fichiers sources. L'analyseur retrouve automatiquement les chemins d'accès aux données. Le datastore est administré grâce à GDAL/OGR.

### Fonds de carte
Les couches de tuile TMS/WMTS/WMS (OSM, IGN etc.) sont détectées et enregistrées dans le catalogue des fonds de carte.

# Ressources
|   |   |
|---|---|
| <b>Application</b> | WEB  |
| <b>Technologies | Svelte JS, Slim Framework, PostgreSQL, SQLite  |
| <b>Dépendances</b>  | QGIS Server, GDAL/OGR, Chromium  |



# <a name="install"></a>Installation
Pour installer Sigapp, la configuration ci-dessous est requise :
- Git
- QGIS Server >= 3.4
- Apache 2.4 (ou autre serveur HTTP)
- PHP >= 7.2
- Composer
- Node.js / npm
- Chromium / Google Chrome (= service d'impression)
- ogr2ogr

Lancer les commandes suivante depuis le répertoire souhaité :
~~~~bash
# Git
git clone https://gitlab.com/geoflux/sigapp.git
cd sigapp
~~~~
- Créer une base de données sous PostgreSQL.<br>
- Configurer les paramètres dans le fichier ".env.example" à la racine du dossier et le renommer ".env"<br>

Puis, exécuter les commandes suivantes :
~~~~bash
# PHP (dependencies + migrations)
composer install
./vendor/bin/phinx migrate -c api/config/config-phinx.php
./vendor/bin/phinx seed:run -c api/config/config-phinx.php

# NPM
npm install
npm run build
~~~~

### Exemple de configuration pour Apache 2.4 :
~~~~apache
Define WEBROOT "path/to/webroot"

# Client :
Alias /sigapp "${WEBROOT}/sigapp/public/"

# API :
Alias /sigapp.api/ "${WEBROOT}/sigapp/api/public/"
<Directory "${WEBROOT}/sigapp/api/public/">
	Options -Indexes +FollowSymLinks +ExecCGI
	AllowOverride All
	Require all granted
	RewriteEngine On
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteBase /sigapp.api/
	RewriteRule ^ index.php [QSA,L]
</Directory>
~~~~

### Générer la doc API :
~~~~apache
# installation :
composer global require zircote/swagger-php

# Génération du fichier openapi.json
openapi --format json --output ./public/docs/openapi.json --bootstrap ./api/config/bootstrap.php ./api/public ./api/routes
~~~~