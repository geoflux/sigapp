import Portals from './Portals.svelte'
import Map from './Map.svelte'
import Manager from './Manager.svelte'
import Login from 'auth/Login.svelte'
import NotFound from './NotFound.svelte'

export const routes = {
    '/': Portals,
    '/portals': Portals,
    '/map': Map,
    '/manager/*': Manager,
    '/login': Login,
    '/*': NotFound
}