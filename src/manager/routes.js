import ProjectLoader from './ProjectLoader.svelte'
import Projects from './Projects.svelte'
import Layers from './Layers.svelte'
import Basemaps from './Basemaps.svelte'
import PortalsMaps from './PortalsMaps.svelte'
import Settings from './Settings.svelte'
import Datasources from './Datasources.svelte'
import NotFound from './NotFound.svelte'

export const routes = {
    '/manager/project-loader': ProjectLoader,
    '/manager/projects': Projects,
    '/manager/layers': Layers,
    '/manager/basemaps': Basemaps,
    '/manager/portals-maps': PortalsMaps,
    '/manager/settings': Settings,
    '/manager/datasources': Datasources,
    '/manager/*': NotFound
}