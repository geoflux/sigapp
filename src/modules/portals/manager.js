import { portals } from 'app/stores.js'
import Sortable from 'sortablejs'    

let $portals
portals.subscribe(obj => { $portals = obj })

async function updateStore () {
    const response = await fetch(`API_URL/portals`)
    const json = await response.json()
    portals.update(elems => elems = json)
}

async function getMaps(portal) {
    const response = await fetch(`API_URL/portals/${portal.id}`)
    const json = await response.json()
    return json.maps
}

async function createPortal(portal){
    const response = await fetch(`API_URL/portals`, {
       method: 'POST',
       headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
       body: JSON.stringify(portal)
   })
   const json = await response.json()
   $portals.push(json)
   portals.update(arr => arr = $portals)
}

async function sort() {
    const element = document.querySelector('.sortable')
    new Sortable(element, {
        preventOnFilter: false,
        handle: '.handle',
        animation: 300,
        easing: "cubic-bezier(1, 0, 0, 1)",
        ghostClass: "z-depth-3",
        onEnd: async (e) => {
            const newOrder = []
            const order = Array.from( e.to.querySelectorAll('header') )
            order.forEach( el => newOrder.push( parseInt(el.dataset.id)) )
            $portals.sort((a, b) => (newOrder.indexOf(a.id) - newOrder.indexOf(b.id)) )
            fetch(`API_URL/portals/reorder/from-array`, {
                method: 'PUT',
                headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
                body: JSON.stringify(newOrder)
            })
            return
        },
    })    
}

async function setOwnerPortal(portal) {
    const response = await fetch(`API_URL/portals/${portal.id}/owner`, {
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        method: 'PUT'
    })
    const json = await response.json()
    $portals.forEach(item => item.owner = false)
    let updated = $portals.find(item => item.id === portal.id)
    updated.owner = true
    portals.update(store => store = $portals)
    return json
}

async function updatePortal(portal){
    const response = await fetch(`API_URL/portals/${portal.id}`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        body: JSON.stringify(portal)
    })
    const json = await response.json()
    let updated = $portals.find(item => item.id === portal.id)
    updated = portal
    portals.update(arr => arr = $portals)
    return json
}

async function deletePortal(portalId){
    const response = await fetch(`API_URL/portals/${portalId}`, {
        headers: {'Accept' : 'application/json'},
        method: 'DELETE'
   })
   const json = await response.json()
   portals.update(arr => arr = arr.filter(item => item.id !== portalId))
   return json
}

export { updateStore, getMaps, createPortal, updatePortal, deletePortal, sort, setOwnerPortal }
