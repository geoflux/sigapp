import { canvas, displayed, maplayers } from 'app/stores.js'
import { displayPortals, updateSlider } from 'app/display.js'
import Layer from 'app/layers/layer.js'
import Basemap from 'app/basemaps/basemap.js'

let newLayer, $maplayers, $displayed
maplayers.subscribe(store => { $maplayers = store })
displayed.subscribe(store => { $displayed = store })
canvas.subscribe(store => { $canvas = store })

export default class MapLayers {

    async setMap(mapID) {
        const response = await fetch(`API_URL/maps/${mapID}`)
        const json  = await response.json()
        const map = json
        this.removeLayers()
        this.addLayers( map.layers )
        if ( map.basemap ) {
            const basemap = new Basemap(map.basemap)
            basemap.setToMap()
        }
        $canvas.fit(map.extent)
        displayPortals()
        updateSlider('layers')
        this.setMapTitle(map.title)
    }

    setMapTitle(title) {
        $displayed.portals.map = title
    }

    addLayers(layers){
        layers.reverse().forEach(layer => {
            newLayer = new Layer(layer)
            maplayers.update(store => [newLayer, ...store])
        })
    }

    removeLayers() {
        $maplayers.forEach(layer => layer.remove())
        maplayers.update(store => store = [])
    }
}