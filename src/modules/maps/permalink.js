import { setMap, getMap } from 'app/maps/maplayers.js'
import { permalink } from 'app/stores.js'


const createPermalink = async () => {
    const token = Math.random().toString(36).substr(2, 9)
    fetch(`API_URL/permalinks/${token}`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        body: JSON.stringify( getMap() )
    })
    permalink.update(store => store = `${window.location.href}?maptoken=${token}`)
    return token
}

const setPermalink = async (params) => {
    let map
    if (params.maptoken) {
        map  = await getMapFromToken(params.maptoken)
    } 
    else if (params.mapid) {
        map  = await getMapById(params.mapid)
    }
    else {
        return false
    }
    if (params.extent) {
        map.canvas = null
        map.extent = setExtentParam(params.extent)
    }
    map.layers = (params.filters) ? setFiltersParam(params.filters, map.layers) : map.layers
    setMap(map, typeof params.printer !== undefined)
    return map
}

const getMapFromToken = async (token) => {
    const response = await fetch(`API_URL/permalinks/${token}`)
    const map  = await response.json()      
    return map 
}

const getMapById = async (id) => {
    const response = await fetch(`API_URL/maps/${id}`)
    const map  = await response.json()      
    return map 
}

const setExtentParam = (param) => {
    const coord = param.split(',')
    return {
        xmin: coord[0],
        ymin: coord[1],
        xmax: coord[2],
        ymax: coord[3],
        srid: 3857
    }
}

const setFiltersParam = (param, layers) => {
    param.split(',').forEach(item => {
        let kv = item.split(': ')
        let layer = layers.find(layer => layer.name === kv[0])
        if ( layer ) {
            layer.filter = kv[1]
        }
    })
    return layers
}


export { createPermalink, setPermalink }