import { user } from 'auth/stores.js'
import { portals } from 'app/stores.js'
import { getMap } from 'app/maps/maplayers.js'
import Sortable from 'sortablejs'    

let $portals, $user
portals.subscribe( store => $portals = store )
user.subscribe(store => { $user = store })

async function updatePortal (portalId) {
    const response = await fetch(`API_URL/portals/${portalId}`)
    const json = await response.json()
    let portal = $portals.find( item => item.id === portalId )
    portal.maps = json.maps
    portals.update( store => store = $portals )
}

async function sort() {
    const element = document.querySelector('.sortable-maps')
    new Sortable(element, {
        preventOnFilter: false,
        handle: '.handle-map',
        animation: 300,
        easing: "cubic-bezier(1, 0, 0, 1)",
        ghostClass: "z-depth-3",
        setData: (dataTransfer, dragEl) => {
            dataTransfer.setData('Text', JSON.stringify({
                id: parseInt(dragEl.dataset.id), 
                portal_id: parseInt(dragEl.dataset.portal), 
            }))
        },
        onEnd: (e) => {
            const newOrder = []
            const order = Array.from( e.to.querySelectorAll('section') )
            order.forEach( el => newOrder.push( parseInt(el.dataset.id)) )
            fetch(`API_URL/maps/reorder/from-array`, {
                method: 'PUT',
                headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
                body: JSON.stringify(newOrder)
            })
            return
        },
    })    
}

async function updateMap(map){
    const response = await fetch(`API_URL/maps/${map.id}`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        body: JSON.stringify(map)
    })
    const json = await response.json()
    updatePortal(map.portal_id)
    return json
}

async function saveMap(map){
    const response = await fetch(`API_URL/maps`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        body: JSON.stringify(map)
   })
   const json = await response.json()
   return json
}

async function updateMapPreview(map){
    const response = await fetch(`API_URL/maps/${map.id}/preview`, {
       method: 'PUT',
       headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
       body: JSON.stringify({
           url: `${window.location.href}?printer=true&mapid=${map.id}`,
           jwt: $user.jwt
        })
   })
    if (response.status !== 200) {
        console.log( await response.json())
        return
    }
   const json = await response.json()
   console.log(json)
   return json
}

async function updateMapViews(map){
    const response = await fetch(`API_URL/maps/${map.id}/views`, {
        headers: {'Accept' : 'application/json' },
        method: 'PUT',
   })
   const json = await response.json()
   return json
}

function setMapPortal(map, portal_id){
    if (map.portal_id !== portal_id) {
        fetch(`API_URL/maps/${map.id}/portal/${portal_id}`, {
            headers: {'Accept' : 'application/json' },
            method: 'PUT'
        })
        const movedFrom = $portals.findIndex(item => item.id === map.portal_id)
        const addedTo = $portals.findIndex(item => item.id === portal_id)
        const dropped = $portals[movedFrom].maps.find(item => item.id === map.id)
        $portals[addedTo].maps.push(dropped)
        $portals[movedFrom].maps = $portals[movedFrom].maps.filter(item => item.id !== dropped.id)
        portals.update(store => store = $portals)
    }
}

async function exportMap(title, inExtent = true) {
    const map = getMap()
    map.title = title
    map.inExtent = inExtent
    const response = await fetch(`API_URL/maps/export/qgis`, {
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        method: 'PUT',
        body: JSON.stringify(map)
   })
   const json = await response.json()
   return json
}

async function deleteMap(map){
    const response = await fetch(`API_URL/maps/${map.id}`, {
        headers: {'Accept' : 'application/json' },
        method: 'DELETE'
   })
   const json = await response.json()
   return json
}

export { updateMap, saveMap, deleteMap, updateMapPreview, updateMapViews, setMapPortal, sort, updatePortal, exportMap }
