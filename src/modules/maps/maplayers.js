import { user } from 'auth/stores.js'
import { settings, displayed, maplayers, layersLegend, canvas, basemaps } from 'app/stores.js'
import { updateSlider } from 'app/display.js'
import Layer from 'app/layers/layer.js'

let $settings, $maplayers, $displayed, $canvas, $basemaps, $user
maplayers.subscribe(store => { $maplayers = store })
displayed.subscribe(store => { $displayed = store })
canvas.subscribe(store => { $canvas = store })
basemaps.subscribe(store => { $basemaps = store })
settings.subscribe(store => { $settings = store })
user.subscribe(store => { $user = store })

const addLayers = function(layers){
	layers.reverse().forEach(layer => {
		const newLayer = new Layer(layer)
		maplayers.update(store => [newLayer, ...store].sort((a, b) => parseInt(b.sort) - parseInt(a.sort)) )
		layersLegend.update(store => store = $maplayers)
	})
}

const removeLayers = () => {
	$maplayers.forEach(layer => layer.remove())
	maplayers.update(store => store = [])
	layersLegend.update(store => [])
}

const setMapById = async (id) => {
	const response = await fetch(`API_URL/maps/${id}`)
	const map  = await response.json()
	setMap(map)
}

const setMap = async (map, print = false) => {
	removeLayers()
	addLayers( map.layers )
	if (map.basemap_id > 0) {
		const basemap = $basemaps.find(item => item.id === map.basemap_id)
		if (typeof basemap === 'undefined') {
			basemap = $basemaps[0]
		}
		basemap.overlays = (map.overlays === null) ? basemap.overlays : map.overlays
		basemap.opacity = map.opacity
		basemap.visible = map.visible
		basemap.print = (print) ? true : false
		basemap.setToMap()
	}
	$canvas.fitToMapSettings(map)
	updateSlider('layers')
	setTitle( map.title )
}

const setTitle = (title) => {
	$displayed.portals.map = title
}

const setNewMap = () => {
	removeLayers()
	if ($basemaps.length > 0) {
		let basemap = $basemaps.find(item => item.main === true)
		if (typeof basemap === 'undefined') {
			basemap = $basemaps[0]
		}
		basemap.setToMap()
	}
	$canvas.fit( $settings.extent )
	setTitle( 'Nouvelle carte ...' )
}

const getMap = () => {
	const basemap = $basemaps.find(item => item.active === true)
	const layers = []
	$maplayers.forEach(layer => {
		delete layer.ol 
		layers.push(layer)
	})
	const map = {
		extent: $canvas.getExtent(),
		canvas: {
			zoom: $canvas.map.getView().getZoom(),
			center: $canvas.map.getView().getCenter(),
			zoomFactor: $canvas.map.getView().zoomFactor_,
		},
		srid: 3857,
		basemap_id: basemap.id,
		opacity: basemap.opacity,
		visible: basemap.visible,
		overlays: basemap.overlays,
		layers: layers
	}
	if ($displayed.printer.active) {
		map.type = 'print'
		map.print = $displayed.printer
	}
	return map
}

const printMap = async (token) => {
    const response = await fetch(`API_URL/permalinks/${token}/print`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        body: JSON.stringify({
			url: `${window.location.href}?printer=true&maptoken=${token}`,
			jwt: $user.jwt
		})
	})
    if (response.status !== 200) {
		const json = await response.json()
        console.log(json)
        return json
    }
	const json = await response.json()
	return json
}

export {addLayers, removeLayers, setMapById, setMap, setNewMap, getMap, printMap};