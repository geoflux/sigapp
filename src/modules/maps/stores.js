import { writable } from 'svelte/store'

export const maps = writable(null)
export const current = writable()