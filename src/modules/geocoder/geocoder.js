import { canvas } from 'app/stores.js'
import { geocoder } from 'app/geocoder/stores.js'

let $canvas, $geocoder
canvas.subscribe(store => { $canvas = store })
geocoder.subscribe(store => { $geocoder = store })

export default class Geocoder {
    constructor(context = ' ') {
        this.query = ''
        this.locations = false
        this.visible = false
        this.relocation = undefined
        this.context = context
    }

    async getLocations(query) {
        const coord = $canvas.getCenterToLatLon()
        const response = await fetch(`https://api-adresse.data.gouv.fr/search/?q=${query}&limit=30&lat=${coord[0]}&lon=${coord[1]}`)
        const result  = await response.json()
		if (!result) {
			return []
        }
        const locations = []
        result.features.forEach(feature => {
            if ( feature.properties.context.indexOf(this.context) > 0 ) {
                locations.push( this.format(feature) )
            }
        })
        return locations.slice(0, 10)
    }

    async getLocationsFromAdditionalAPI(query) {
        // console.log(query)
        const response = await fetch(`${'GEOCODER_API_URL'}?q=${query}`, {
            headers: { 'Accept' : 'application/json'}
        })
        const result  = await response.json()
        if (!result) {
            return []
        }
        return result.slice(0, 5)
    }

    format(feature) {
        let result = feature.properties.name  + ' ' + feature.properties.postcode  + ' ' + feature.properties.city
        let type, zoom
        switch(feature.properties.type)  {
            case 'housenumber' : type = 'Adresse'; zoom = 19; break;
            case 'municipality' : result = feature.properties.name; type = 'Commune'; zoom = 13; break;
            case 'locality' : type = 'Lieu-dit'; zoom = 15; break;
            case 'street' : type = 'Voie'; zoom = 18; break;
        }
        return {
            lat: feature.geometry.coordinates[1],
            lon: feature.geometry.coordinates[0],
            result: result,
            context: feature.properties.context,
            type: type,      
            zoom: zoom     
        }
    }

    setOverlay(location) {
        const overlay = $canvas.createOverlay({
            element: document.getElementById('location-overlay'),
            autoPan: false,
            autoPanAnimation: {
                duration: 0
            }
        })
        const coordinates = $canvas.locate(location.lat, location.lon, location.zoom)
        $canvas.map.addOverlay(overlay)
        overlay.setPosition(coordinates)
        geocoder.update(store => store = {active: true, content: location, overlay: overlay, locations: []})
    }

    relocate() {
        if ($geocoder.content.zoom) {
            this.setOverlay($geocoder.content)
            return
        }
        return false
    }
}