import { writable } from 'svelte/store'

export const geocoder = writable({active: false, content: {result: null}, overlay: null, locations: []})