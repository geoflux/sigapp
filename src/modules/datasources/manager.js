
async function getDatasources() {
    const response = await fetch(`API_URL/datasources`);
    return await response.json()
}

async function deleteDatasources() {
    const response = await fetch(`API_URL/datasources`, {
        headers: {'Accept' : 'application/json' },
        method: 'DELETE'
    })
    const json = await response.json()
    return json
}

export { getDatasources, deleteDatasources }