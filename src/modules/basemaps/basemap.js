import { canvas, basemaps } from 'app/stores.js'
import XYZ from 'ol/source/XYZ'
import {Tile as TileLayer} from 'ol/layer'
import WMTS from 'ol/source/WMTS'
import WMTSTileGrid from 'ol/tilegrid/WMTS'

let $canvas, $basemaps
canvas.subscribe(value => { $canvas = value })
basemaps.subscribe(value => { $basemaps = value })

export default class Basemap {
    constructor(basemap) {
        for ( const [key, value] of Object.entries(basemap) ) {
            this[key] = value
        }
		this.opacity = (typeof basemap.opacity === 'undefined') ? 1 : basemap.opacity
		this.visible = (typeof basemap.visible === 'undefined') ? true : basemap.visible
    }

    updateStore() {
        // let updated = $basemaps.find( item => item.id === this.id ) 
        // updated = this
        basemaps.update(store => store = $basemaps)
    }

    setToMap() {
        $basemaps.forEach(item => item.active = false)
        $canvas.map.removeLayer($canvas.basemap)
        this.removeOverlays()
        $canvas.basemap = this.getTileLayer(this)
        $canvas.map.getLayers().insertAt(0, $canvas.basemap)
        if (this.overlays !== null) {
            this.overlays.forEach(overlay => {
                this.setOverlay(overlay)
            })
        }
        this.setVisible( this.visible )
        this.setOpacity( this.opacity )
        this.active = true
        this.updateStore()
    }

    setOverlay(overlay) {
        if (overlay.checked) {
            overlay.uuid = Math.random().toString(36).substr(2, 9)
            this.overlays[overlay.uuid] = this.getTileLayer(overlay)
            this.overlays[overlay.uuid].setZIndex(100)
            $canvas.addToLayerGroup( 'Overlays', this.overlays[overlay.uuid] )
        }
        else {
            if (overlay.uuid) {
                $canvas.removeFromLayerGroup( 'Overlays', this.overlays[overlay.uuid] )
            }
        }
        $canvas.map.updateSize()
    }

    removeOverlays() {
        $canvas.clearLayerGroup('Overlays')
    }

    getTileLayer(basemap) {
        if (basemap.service === 'WMTS') {
            return this.getWMTSTileLayer(basemap)
        } 
        else if (basemap.service === 'TMS') {
            return new TileLayer({
                source: new XYZ({
                    url: basemap.url,
                    attributions : basemap.attributions,
                    crossOrigin: basemap.print ? null : 'Anonymous',
                })
            })
        }
        return basemap.service
    }

    getWMTSTileLayer(basemap) {
        return new TileLayer({
            source : new WMTS({
                url: basemap.url,
                attributions: basemap.attributions,
                crossOrigin: basemap.print ? null : 'Anonymous',
                layer: basemap.layer,
                matrixSet: 'PM',
                format: basemap.format,
                tileGrid : new WMTSTileGrid({
                    origin: [-20037508,20037508], // topLeftCorner
                    resolutions: [156543.03392804103,78271.5169640205,39135.75848201024,19567.879241005125,9783.939620502562,4891.969810251281,2445.9849051256406,1222.9924525628203,611.4962262814101,305.74811314070485,152.87405657035254,76.43702828517625,38.218514142588134,19.109257071294063,9.554628535647034,4.777314267823517,2.3886571339117584,1.1943285669558792,0.5971642834779396,0.29858214173896974,0.14929107086948493,0.07464553543474241], // résolutions
                    matrixIds: ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"] // ids des TileMatrix
                }),
                style: basemap.style
            })
        })
    }
    
    setOpacity( value ) {
        this.opacity = parseFloat(value)
        $canvas.basemap.setOpacity( this.opacity )
        this.updateStore()
    }

    setVisible( option ) {
        this.visible = option
        $canvas.basemap.setVisible( this.visible )
        this.updateStore()
    }
}