import { writable } from 'svelte/store'

export const current = writable()
export const manager = writable()
export const collection = writable()