import { basemaps } from 'app/stores.js'
import { manager } from 'app/basemaps/stores.js'
import Basemap from 'app/basemaps/basemap.js'
import Sortable from 'sortablejs'

let $manager, $basemaps
manager.subscribe(store => $manager = store )
basemaps.subscribe(store => $basemaps = store )

async function getBasemaps() {
    const response = await fetch(`API_URL/basemaps`)
    const json = await response.json()
    manager.update(store => store = json)
    return $manager
}

function updateStore () {
    const arr = []
    $manager.displayed.forEach( item => {
        arr.push(new Basemap(item))
    })
    basemaps.update(store => store = arr)
    const basemap = $basemaps.find(item => item.main === true)
    if ( typeof basemap !== 'undefined' ) {
        basemap.setToMap()
    }
}

function sort(collection) {
    const element = document.querySelector('.sortable')
    new Sortable(element, {
        preventOnFilter: false,
        handle: '.handle',
        animation: 300,
        easing: "cubic-bezier(1, 0, 0, 1)",
        ghostClass: "z-depth-3",
        setData: (dataTransfer, dragEl) => {
            dataTransfer.setData('Text', JSON.stringify({id: parseInt(dragEl.dataset.id), collection: dragEl.dataset.collection}) )
        },
        onEnd: (e) => {
            const newOrder = []
            const order = Array.from( e.to.querySelectorAll('article') )
            order.forEach( el => newOrder.push( parseInt(el.dataset.id)) )
            $manager[collection].sort((a, b) => (newOrder.indexOf(a.id) - newOrder.indexOf(b.id)) )
            fetch(`API_URL/basemaps/reorder/from-array`, {
                method: 'PUT',
                headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
                body: JSON.stringify(newOrder)
            })
            return $manager
        },
    })    
}

async function updateBasemap(basemap){
    let updated = $manager[basemap.collection].find(item => item.id === basemap.id)
    updated = basemap
    manager.update(store => store = $manager)
    const response = await fetch(`API_URL/basemaps/${basemap.id}`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        body: JSON.stringify(basemap)
    })
    const json = await response.json()
    return json
}

function setBasemapCollection(basemap, collection){
    if (basemap.collection !== collection) {
        fetch(`API_URL/basemaps/${basemap.id}/collection/${collection}`, {
            method: 'PUT'
        })
        basemap = $manager[basemap.collection].find(item => item.id === basemap.id)
        $manager[basemap.collection] = $manager[basemap.collection].filter( (item) => { return item.id !== basemap.id })
        basemap.collection = collection
        $manager[collection].splice(basemap.sort, 0, basemap)
        manager.update(store => store = $manager)
    }
}

async function setMainBasemap(basemap){
    const response = await fetch(`API_URL/basemaps/${basemap.id}/main`, {
        headers: {'Accept' : 'application/json'},
        method: 'PUT'
    })
    const json = await response.json()
    $manager.displayed.forEach(item => item.main = false)
    $manager.stored.forEach(item => item.main = false)
    $manager.overlayed.forEach(item => item.main = false)
    let updated = $manager.displayed.find(item => item.id === basemap.id)
    updated.main = true
    manager.update(store => store = $manager)
    return json
}

async function setBasemapOverlay(basemap, overlay){
    const response = await fetch(`API_URL/basemaps/${basemap.id}/overlays`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        body: JSON.stringify(overlay)
    })
    const json = await response.json()
    let updated = $manager[basemap.collection].find(item => item.id === basemap.id)
    updated.overlays = json
    manager.update(store => store = $manager)
    return json
}

async function deleteBasemapOverlay(basemap, overlayIndex){
    const response = await fetch(`API_URL/basemaps/${basemap.id}/overlays/${overlayIndex}`, {
        method: 'DELETE'
    })
    const json = await response.json()
    let updated = $manager[basemap.collection].find(item => item.id === basemap.id)
    updated.overlays = json
    manager.update(store => store = $manager)
    console.log(json)
    return json
}

async function setBasemapPreview(basemap, data) {
    const response = await fetch(`API_URL/basemaps/${basemap.id}/preview`, {
        method: 'PUT',
        headers: {'Content-Type' : 'text/plain', 'Accept' : 'application/json' },
        body: data
    })
    const json = await response.json()
    let updated = $manager[basemap.collection].find(item => item.id === basemap.id)
    updated = json
    manager.update(store => store = $manager)
    console.log(json)
    return json
}

function deleteBasemap(basemap){
    fetch(`API_URL/basemaps/${basemap.id}`, {
        headers: {'Accept' : 'application/json' },
        method: 'DELETE'
    })
    $manager[basemap.collection] = $manager[basemap.collection].filter(item => item.id !== basemap.id)
    manager.update(store => store = $manager)
}

function getTileLayer(basemap) {

}

export { 
    getBasemaps, updateStore, sort, updateBasemap, setBasemapCollection, 
    setBasemapOverlay, deleteBasemapOverlay, setMainBasemap, getTileLayer, 
    setBasemapPreview, deleteBasemap 
}
