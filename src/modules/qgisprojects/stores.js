import { writable } from 'svelte/store'

export const options = writable({
    folder_id: 0,
    role_id: 5,
})

export const current = writable()

export const reload = writable(false)