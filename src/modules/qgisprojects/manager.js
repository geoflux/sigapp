import { options} from 'app/qgisprojects/stores.js'
import Sortable from 'sortablejs'

async function getProjects() {
    const response = await fetch(`API_URL/qgis-projects`);
    return await response.json()
}

async function loadProject(file, userOptions) {
    const formData = new FormData()
    formData.append('options', JSON.stringify(userOptions) )
    formData.append('file',  file, file.name)
    const response = await fetch(`API_URL/qgis-projects`, {
        headers: {'Accept' : 'application/json'},
        method: "POST",
        body: formData
    })
    if (response.status !== 200) {
        const json = response.json()
        console.log(json)
        return {error: json}
    }
    const json = await response.json()
    options.update(store => store = {folder_id: 0, role_id: 5})
    return json
}

function sortProjectLayers() {
    const element = document.querySelector('.sortable-project-layers')
    new Sortable(element, {
        preventOnFilter: false,
        handle: '.handle',
        animation: 300,
        easing: "cubic-bezier(1, 0, 0, 1)",
        ghostClass: "z-depth-3",
        setData: (dataTransfer, dragEl) => {
            dataTransfer.setData('Text', JSON.stringify({id: parseInt(dragEl.dataset.id)}) )
        }
    })    
}

async function updatePreviews(projectName) {
    const response = await fetch(`API_URL/qgis-projects/${projectName}/previews`, {
        headers: {'Accept' : 'application/json'},
        method: "PUT"
    })
    const json = await response.text()
    return json
}

async function deleteProject(project) {
    const response = await fetch(`API_URL/qgis-projects/${project.name}`, {
        headers: {'Accept' : 'application/json'},
        method: 'DELETE'
    })
    return await response.json()
}

export { getProjects, loadProject, updatePreviews, sortProjectLayers, deleteProject }
