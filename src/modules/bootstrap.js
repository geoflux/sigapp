import { settings, basemaps, portals, gis, map } from 'app/stores.js'
import Basemap from 'app/basemaps/basemap.js'
import { setPermalink } from './maps/permalink'
import { getAccount } from 'auth/account.js'

export default class Bootstrap {
    constructor() {
        this.params = this.getParams()
    }

    async load() {
        if (typeof this.params.printer === 'undefined') {
            await getAccount()
            await this.getSettings()
            await this.getPortals()
            await this.getBasemaps()
            this.getGis()
            if ( this.params.maptoken ) {
                setPermalink(this.params)
            }
            return 'application'
        }
        else {
            await this.getBasemaps()
            const printMap = await setPermalink(this.params)
            map.update(store => store = printMap )
            return 'printer'
        }
    }

    getParams() {
        const params = []
        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, (kv, key, value) => {
            params[key] = decodeURI(value)
        })
        return params
    }

    async getSettings() {
        const response = await fetch(`API_URL/settings`)
        const json  = await response.json()
        if (typeof json.extent === 'undefined') {
            json.extent = {xmin: -582144.4074, ymin: 5183042.0140, xmax: 927028.2790, ymax: 6670200.8363, srid: 3857}
        }
        settings.update(store => store = json)
    }

    async getPortals() {
        const response = await fetch(`API_URL/portals`)
        const json  = await response.json()
        portals.update(store => store = json)        
    }

    async getBasemaps() {
        const response = await fetch(`API_URL/basemaps`)
        const json = await response.json()
        if (json.displayed.length > 0) {
            const arr = []
            json.displayed.forEach( item => {
                arr.push( new Basemap(item) )
            })
            const basemap = arr.find(item => item.main === true)
            if ( typeof basemap !== 'undefined' ) {
                basemap.setToMap()
            }
            basemaps.update(store => store = arr)
        }
    }

    async getGis() {
        const response = await fetch(`API_URL/gis`)
        const json = await response.json()
        gis.update(store => store = json)
    }
}