import { settings } from 'app/stores.js'

const updateSettingsName = async (name) => {
    const response = await fetch(`API_URL/settings`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        body: JSON.stringify({name: name})
    })
    const json = await response.json()
    console.log(json)
    settings.update(store => store = json)
}

const updateSettingsLogo = async (file) => {
    if(!file){
        return false
    }
    const formData = new FormData()
    formData.append('file',  file, file.name)
    const response = await fetch(`API_URL/settings/logo`, {
        headers: {'Accept' : 'application/json'},
        method: "POST",
        body: formData
    })
    const json = await response.json()
    console.log(json)
    settings.update(store => store = json)
}

const updateSettingsOverview = async (layer_id) => {
    const response = await fetch(`API_URL/settings/overview/${layer_id}`, {
        headers: {'Accept' : 'application/json'},
        method: 'PUT'
    })
    const json = await response.json()
    settings.update(store => store = json)
}

export { updateSettingsName, updateSettingsLogo, updateSettingsOverview }