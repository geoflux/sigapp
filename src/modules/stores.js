import { writable } from 'svelte/store';
import Canvas from 'app/canvas/canvas.js';

export const settings = writable([])
export const loader = writable({active: false, message: '', submessage: '', color: 'ui'})
export const managerPage = writable({icon: 'file_upload', name: 'Importer un projet QGIS', location: '/manager/project-loader'})
export const canvas = writable(new Canvas)
export const basemaps = writable([])
export const portals = writable([])
export const gis = writable([])
export const maplayers = writable([])
export const layersLegend = writable([])
export const layer = writable(false)
export const map = writable('Nouvelle carte')
export const tools = writable({})
export const pointer = writable({
    uuid: null,
    active: false,
    promise: false,
    layer: false
})
export const contexts = writable([
    {name: 'minimap', tooltip: 'Carte de situation', control: null},
    {name: 'annotation', tooltip: 'Coordonnées du curseur', control: null},
    {name: 'scale', tooltip: 'Échelle', control: null},
    {name: 'title', tooltip: 'Titre de la carte', control: null},
    {name: 'attribution', tooltip: 'Attributions', control: null},
]);
export const permalink = writable('')
// export const displayed = writable({
//     header: {nav: true, openPortals: true, openManager: true, location: true},
//     nav: {nav: true, slider: ''},
//     portals: {portals: false, active: false, map: 'Nouvelle carte...'},
//     footer: {datatable: false, size: 0, uuid: null, left: 0},
//     manager: false,
//     canvas: {left: 0, bottom: 0, cursor: 'navigate'},
//     printer: {active: false, legend: true, title: '', logo: false, dimensions: 'A4', format: 'PNG', orientation: 'landscape'}
// })

/* PORTAL */
export const displayed = writable({
    header: {nav: false, openPortals: true, openManager: false, location: false},
    nav: {nav: false, slider: ''},
    portals: {portals: true, active: true, map: 'Nouvelle carte...'},
    footer: {datatable: false, size: 0, uuid: null, left: 0},
    manager: false,
    canvas: {left: 0, bottom: 0, cursor: 'navigate'},
    printer: {active: false, legend: true, title: '', logo: false, sizing: ['21cm', '29.7cm']}
})

/* MANAGER */
// export const displayed = writable({
//     header: {nav: false, openPortals: false, openManager: true, location: false},
//     nav: {nav: true, slider: 'false'},
//     portals: {portals: false, active: false, map: 'Nouvelle carte...'},
//     footer: {size: false, uuid: null, left: 0},
//     manager: true,
//     canvas: {left: 0, bottom: 0, cursor: 'navigate'},
//     printer: {active: false, legend: true, title: '', logo: false, height: '21cm', width: '29.7cm'}
// })