import { settings, canvas } from 'app/stores.js'
import Stamen from 'ol/source/Stamen'
import ImageLayer from 'ol/layer/Image'
import ImageWMS from 'ol/source/ImageWMS'
import Control from 'ol/control/Control'
import Attribution from 'ol/control/Attribution'
import ScaleLine from 'ol/control/ScaleLine'
import OverviewMap from 'ol/control/OverviewMap'
import MousePosition from 'ol/control/MousePosition'
import { createStringXY } from 'ol/coordinate'

let $canvas, $settings
canvas.subscribe(value => { $canvas = value })
settings.subscribe(value => { $settings = value })

export default class Context {
    constructor (name) {
        this.name = name
    }

    getControl() {
        if (this.name === 'minimap') {
            return this.overviewMap()
        }
        else if (this.name === 'annotation') {
            return this.mousePosition()
        }
        else if (this.name === 'scale') {
            return this.scaleLine()
        }
        else if (this.name === 'title') {
            return this.mapTitle()
        }
        else if (this.name === 'attribution') {
            return this.attributions()
        }
    }

    mapTitle() {
        this.context =  new Control({
            element: document.querySelector('#map-title')
        })
        return this
    }

    attributions() {
        this.context =  new Attribution()
        return this
    }

    scaleLine() {
        this.context =  new ScaleLine()
        return this       
    }

    overviewMap() {
        let layer = null
        if ($settings.layername) {
            layer = new ImageLayer({
                opacity: 1,
                visible: true,
                zIndex: 99,
                source: new ImageWMS({
                    url: 'QGIS_SERVER_URL',
                    params: {
                        'MAP' : $settings.map ? $settings.map : null,
                        'LAYERS': $settings.layername ? $settings.layername : null,
                        'FORMAT':'image/png',
                        'VERSION': '1.3.1',
                    },
                    serverType: 'qgis',
                })
            })
        }
        this.context = new OverviewMap({
            className: 'ol-overviewmap',
            layers: [
                $canvas.tileLayer({
                    source: new Stamen({layer: 'terrain-background'})
                }),
                layer
            ],
            collapseLabel: '\u00BB',
            label: '\u00AB',
            collapsed: false,
            view: $canvas.createView({
                extent : [
                    $settings.extent['xmin'], 
                    $settings.extent['ymin'], 
                    $settings.extent['xmax'], 
                    $settings.extent['ymax'], 
                ],
                maxZoom: 7,
                minZoom: 12,
                zoom: 10
            })
        })
        return this
    }

    mousePosition() {
        this.context = new MousePosition({
            coordinateFormat: createStringXY(4),
            projection: 'EPSG:4326',
            className: 'ol-custom-mouse-position',
            target: document.querySelector("#mouse-position .position b"),
            undefinedHTML : '<i style="text-align:left;color:#a5d6a7;font-weight:normal;">Hors du cadre</i>'            
        })
        return this
    }

    add() {
        $canvas.map.addControl(this.context)
    }

    remove() {
        $canvas.map.removeControl(this.context)
    }

}