import Map from 'ol/Map.js';
import View from 'ol/View.js';
import { fromLonLat, transform } from 'ol/proj.js';
import { Group as LayerGroup, Tile as TileLayer, Vector as VectorLayer } from 'ol/layer.js'
import { defaults as defaultControls } from 'ol/control.js';
import VectorSource from 'ol/source/Vector'
import Draw from 'ol/interaction/Draw'
import { Circle, Fill, Stroke, Style } from 'ol/style'
import Overlay from 'ol/Overlay'
import { getArea, getLength } from 'ol/sphere'
import { unByKey } from 'ol/Observable'
import GeoJSON from 'ol/format/GeoJSON'

export default class Canvas {
	constructor () {
		this.view = new View ({
			center: fromLonLat([4.530, 46.641]),
			zoom: 9,
			zoomFactor: 2
		})
		this.basemap = new TileLayer ({})
		this.layers = new LayerGroup ({
			layers: [],
			title: 'Layers',
			zIndex: 2
		})
		this.overlays = new LayerGroup ({
			layers: [],
			title: 'Overlays',
			zIndex: 3
		})
		this.drawLayerSource = new VectorSource()
		this.drawLayer = new VectorLayer({
			source: this.drawLayerSource,
			style: new Style({
				fill: new Fill({ color: 'rgba(255, 255, 255, 0.2)' }),
				stroke: new Stroke({ color: '#ffcc33', width: 2 }),
				// image: new Circle({
				// 	radius: 7,
				// 	fill: new Fill({ color: '#ffcc33' })
				// })
			}),
			zIndex: 200
		})
		this.selectionLayerSource = new VectorSource({ features: [] })
		this.selectionLayer = new VectorLayer({
			source: this.selectionLayerSource,
			zIndex: 300,
			style: (feature) => {
				return this.getGeoJsonStyle( feature.getGeometry().getType() )
			}
		})
		this.map = new Map ({
			controls: defaultControls({attribution: false}),
			layers: [
				this.basemap,
				this.layers,
				this.overlays,
				this.drawLayer,
				this.selectionLayer,
			],
			// overlays: [this.popup, this.location],
			// target: 'map',
			loadTilesWhileAnimating: true,
			// loadTilesWhileInteracting: true,
			view: this.view
		})
	}

	getExtent() {
		const extent = this.map.getView().calculateExtent()
		return {
			xmin: extent[0],
			ymin: extent[1],
			xmax: extent[2],
			ymax: extent[3],
			srid: 3857
		}
	}

	focusOut() {
		this.map.getView().setCenter(fromLonLat([4.530, 46.641]));
		this.map.getView().setZoom(9);
	}

	locate(lat, lon, zoom) {
		const coordinates = transform([lon, lat], 'EPSG:4326', 'EPSG:3857');
		this.map.getView().setCenter(coordinates)
		this.map.getView().setZoom(zoom)
		return coordinates
	}

	getCenterToLatLon() {
		return transform(this.map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326')
	}

	setZoomFactor(zoomFactor) {
		this.map.setView( new View ({
			zoomFactor: zoomFactor
		}))
	}

	fit(extent) {
		this.map.getView().fit([
			parseFloat( extent['xmin'] ),
			parseFloat( extent['ymin'] ),
			parseFloat( extent['xmax'] ),
			parseFloat( extent['ymax'] )
		], this.map.getSize() )
		if ( this.map.getView().zoomFactor_ === 2 ) {
			this.map.getView().setMaxZoom(15)
			this.map.getView().setMaxZoom(28)
		}
	}

	fitToMapSettings(map) {
		if (map.canvas !== null) {
			this.setZoomFactor(map.canvas.zoomFactor)
			this.map.getView().setCenter(map.canvas.center)
			this.map.getView().setZoom(map.canvas.zoom)
			return
		}
		else {
			this.fit(map.extent)
			return
		}
	}

    getFormatLength(line) {
        let length = getLength(line)
        let output
        if (length > 100) {
            output = (Math.round(length / 1000 * 100) / 100) + ' ' + 'km'
        } else {
            output = (Math.round(length * 100) / 100) + ' ' + 'm'
        }
        return output
    }

    getFormatArea(polygon) {
        let area = getArea(polygon)
        let output
        if (area > 10000) {
            output = (Math.round(area / 1000000 * 100) / 100) +	' ' + 'km<sup>2</sup>'
        } else {
            output = (Math.round(area * 100) / 100) + ' ' + 'm<sup>2</sup>'
        }
        return output
	}

	addToLayerGroup(groupTitle, layer) {
		this.map.getLayers().forEach(group => {
			if (group.get('title') === groupTitle) {
				group.getLayers().getArray().push( layer )
				this.map.updateSize()
				return
			}
		})
		return false
	}

	removeFromLayerGroup(groupTitle, layer) {
		this.map.getLayers().forEach(group => {
			if (group.get('title') === groupTitle) {
				group.getLayers().array_ = group.getLayers().getArray().filter( item => item !== layer )
				this.map.updateSize()
				return
			}
		})
		return false
	}

	clearLayerGroup(groupTitle) {
		this.map.getLayers().forEach(group => {
			if (group.get('title') === groupTitle) {
				group.getLayers().array_ = []
				this.map.updateSize()
				return
			}
		})
		return false
	}

	createView(options) { return new View(options) }
	createOverlay(options) { return new Overlay(options) }
	tileLayer(options) { return new TileLayer(options) }
	vectorLayer(options) { return new VectorLayer(options) }
	vectorSource(options) { return new VectorSource(options) }
	geoJSON() { return new GeoJSON() }
	draw(options) { return new Draw(options) }
	style(options) { return new Style(options) }
	fill(options) { return new Fill(options) }
	stroke(options) { return new Stroke(options) }
	circle(options) { return new Circle(options) }
	removeEventListener(listener) { unByKey(listener) }

	addGeoJsonFeature(geojson, uuid) {
		let feature = new GeoJSON().readFeature(geojson, { featureProjection: 'EPSG:3857' })
		feature.setId(uuid)
		this.selectionLayerSource.addFeature(feature)
	}

	removeGeoJsonFeature(uuid) {
		if ( this.selectionLayerSource.getFeatureById(uuid) !== null) {
			this.selectionLayerSource.removeFeature(
				this.selectionLayerSource.getFeatureById(uuid)
			)
		}
	}

	getGeoJsonStyle(geometryType) {
		let style = {
			'Point': new Style({
				image: new Circle({
					fill: new Fill({ color: [15,255,161,1] }),
					stroke: new Stroke({ color: [28,255,241,1] }),
					radius: 10
				})
			}),
			'LineString': new Style({
				stroke: new Stroke({
					color: 'rgba(15,255,161,1)',
					width: 8
				})
			}),
			'MultiLineString': new Style({
				stroke: new Stroke({
					color: 'rgba(15,255,161,1)',
					width: 8
				})
			}),
			'MultiPoint': new Style({
				image: new Circle({
					fill: new Fill({ color: [28,255,241,1] }),
					stroke: new Stroke({ color: [15,255,161,1] }),
					radius: 10
				})
			}),
			'MultiPolygon': new Style({
				stroke: new Stroke({
					color: 'rgba(15,255,161,1)',
					width: 5
				}),
				fill: new Fill({
					color: 'rgba(28,255,241,0.0)'
				})
			}),
			'Polygon': new Style({
				stroke: new Stroke({
					color: 'rgba(15,255,161,1)',
					width: 5
				}),
				fill: new Fill({
					color: 'rgba(28,255,241,0.0)'
				})
			}),
			'GeometryCollection': new Style({
				stroke: new Stroke({
					color: 'rgba(28,255,241,0.2)',
					width: 2
				}),
				fill: new Fill({
					color: 'rgba(28,255,241,0.0)'
				}),
				image: new Circle({
					radius: 10,
					fill: null,
					stroke: new Stroke({
						color: 'rgba(28,255,241,0.2)'
					})
				})
			}),
			'Circle': new Style({
				stroke: new Stroke({
					color: 'rgba(28,255,241,0.2)',
					width: 2
				}),
				fill: new Fill({
					color: 'rgba(28,255,241,0.0)'
				})
			})
		}
		return style[geometryType]
	}
}
