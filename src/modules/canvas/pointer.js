import { canvas, pointer, maplayers } from 'app/stores.js'
import { updateSlider } from 'app/display.js'

let $canvas, $pointer, $maplayers
canvas.subscribe(value => { $canvas = value })
pointer.subscribe(value => { $pointer = value })
maplayers.subscribe(value => { $maplayers = value })

export default class Pointer {
    constructor() {
        this.uuid = null
        this.active = false
        this.properties = null
        this.layer = null
        this.setOverlay()
        this.init()
        this.start()
    }

    setOverlay() {
        this.overlay = $canvas.createOverlay({
            element: document.getElementById('pointer-overlay'),
            autoPan: true,
            autoPanAnimation: {
                duration: 100
            }			
        })
        $canvas.map.addOverlay(this.overlay)
    }

    init() {
        updateSlider('layers')
        if ($pointer.uuid === null) {
            if ($maplayers[0]) {
                $pointer.layer = $maplayers[0]
                $pointer.uuid = $maplayers[0].uuid
            } else {
                $pointer.uuid = null
                $pointer.layer = false
            }
        } else {
            $pointer.layer = $maplayers.find(store => store.uuid === $pointer.uuid)
        }
        $pointer.active = true
        pointer.update(store => store = $pointer)
    }

    start() {
        this.listener = $canvas.map.on('click', (e) => {
            $pointer.active = true
            if(this.feature) $canvas.removeGeoJsonFeature(this.feature)
            const coordinates = e.coordinate
            $pointer.promise = $pointer.layer.getFeatureInfoFromCoordinates(
                coordinates[0], coordinates[1] , $canvas.map.getView().getZoom()
            )
            pointer.update(store => store = $pointer)
            $pointer.promise.then(geojson => {
                if(geojson) {
                    this.feature = Math.random().toString(36).substr(2, 9)
                    $canvas.addGeoJsonFeature(geojson, this.feature)
                } else {
                    this.feature = null
                }
                // this.getProperties(geojson)
            })
            this.overlay.setPosition(coordinates)
        })
    }

    stop() {
        if(this.feature) $canvas.removeGeoJsonFeature(this.feature)
        $canvas.removeEventListener(this.listener)
        $pointer.active = false
        $pointer.promise = false
        $pointer.layer = false
        pointer.update(store => store = $pointer)
    }

    // getProperties(geojson) {
    //     this.properties = []
    //     Object.entries(geojson.properties).forEach(item => {
    //         this.properties.push({
    //             key: item[0],
    //             value: item[1]
    //         })
    //     })
    // }

    close() {
        this.overlay.setPosition(undefined)
        if(this.feature) $canvas.removeGeoJsonFeature(this.feature)
        this.feature = null
    }
}