import { canvas, tools } from 'app/stores.js'
import Measure from 'app/canvas/measure.js'
import Pointer from 'app/canvas/pointer.js'
import DragZoom from 'ol/interaction/DragZoom'
import DragRotate from 'ol/interaction/DragRotate'

let $canvas, $tools
canvas.subscribe(store => $canvas = store)
tools.subscribe(store => $tools = store)

export default class Tools {
    constructor (tool) {
        this.name = tool
        this.tooltips = []
        this.removePreviousInteraction().setInteraction()
        if (this.interaction) {
            $canvas.map.addInteraction( this.interaction )
        }
        tools.update(object => object = this)
    }

    removePreviousInteraction() {
        if ($tools.interaction) $canvas.map.removeInteraction( $tools.interaction )
        if ($tools.tooltips && $tools.tooltips.length > 0) {
            $tools.tooltips.forEach(overlay => { 
                $canvas.map.removeOverlay(overlay)
            })
            $canvas.drawLayer.getSource().clear()
        }
        if($tools.pointer) $tools.pointer.stop()
        return this
    }

    setInteraction() {
        if (this.name === 'pointer') {
            this.pointer = new Pointer()
        }
        else if (this.name === 'zoomin') {
            this.interaction = this.dragZoom()
        } 
        else if (this.name === 'distance' || this.name === 'area') {
            let measure = new Measure( this.name )
            this.tooltips = measure.tooltips
            this.interaction = measure.draw
            $canvas.map.removeInteraction(  measure.draw )
        } 
        else if (this.name === 'rotate') {
            this.interaction = this.dragRotate()
        } 
        else {
            this.interaction = false
        }
        return this
    }

    dragZoom() {
        return new DragZoom({
            duration: 500,
            condition: (mapBrowserEvent) => {
                let originalEvent = mapBrowserEvent.originalEvent
                return (
                    !originalEvent.ctrlKey &&
                    !(originalEvent.metaKey || originalEvent.altKey) &&
                    !originalEvent.shiftKey             
                )
            }
        })
    }

    dragRotate() {
        return new DragRotate({
            duration : 500,
            condition: (mapBrowserEvent) => {
                let originalEvent = mapBrowserEvent.originalEvent
                return (
                    !originalEvent.ctrlKey &&
                    !(originalEvent.metaKey || originalEvent.altKey) &&
                    !originalEvent.shiftKey
                )
            }
        })
    }
}