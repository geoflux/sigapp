import { canvas } from 'app/stores.js'

let $canvas
canvas.subscribe(value => { $canvas = value })

export default class Measure {
    constructor (name) {
        this.name = name
        this.type = (this.name === 'area') ? 'Polygon' : 'LineString'
        this.tooltips = []
        this.draw = this.draw()
        $canvas.map.addInteraction(this.draw)
        this.createMeasureTooltip()
        this.drawStartEvent()
        this.drawEndEvent()
    }

    draw() {
        return $canvas.draw({
            source: $canvas.drawLayerSource,
            type: this.type,
            style: $canvas.style({
                fill: $canvas.fill( {color: 'rgba(255, 255, 255, 0.2)'} ),
                stroke:  $canvas.stroke({
                    color: 'rgba(0, 0, 0, 0.5)',
                    lineDash: [10, 10],
                    width: 2
                }),
                image:  $canvas.circle({
                    radius: 5,
                    stroke:  $canvas.stroke( {color: 'rgba(0, 0, 0, 0.7)'} ),
                    fill:  $canvas.fill( {color: 'rgba(255, 255, 255, 0.2)'} )
                })
            })
        })
    }

    createMeasureTooltip() {
        this.tooltipElement = document.createElement('div')
        this.tooltipElement.className = 'tooltip tooltip-measure'
        this.tooltip = $canvas.createOverlay({
            element: this.tooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center'
        });
        $canvas.map.addOverlay(this.tooltip)
        this.tooltips.push(this.tooltip)
    }

    drawStartEvent() {
        this.draw.on('drawstart', (e) => {
            this.sketch = e.feature
            let tooltipCoord = e.coordinate
            this.listener = this.sketch.getGeometry().on('change', (e) => {
                let geom = e.target
                let output
                if (this.name === 'area') {
                    output = $canvas.getFormatArea(geom)
                    tooltipCoord = geom.getInteriorPoint().getCoordinates()
                } else if (this.name === 'distance') {
                    output = $canvas.getFormatLength(geom)
                    tooltipCoord = geom.getLastCoordinate()
                }
                this.tooltipElement.innerHTML = output
                this.tooltip.setPosition(tooltipCoord)
            })
        })
    }

    drawEndEvent() {
        return this.draw.on('drawend', (e) => {
            e.preventDefault()
            this.tooltipElement.className = 'tooltip tooltip-static'
            this.tooltip.setOffset([0, -7])
            this.tooltipElement = null
            this.sketch = null
            this.createMeasureTooltip()
            $canvas.removeEventListener(this.listener)
        })
    }
}