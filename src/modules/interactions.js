import M from 'materialize-css'
import interact from 'interactjs'

const initDropdown = (options = {coverTrigger: false, alignment: 'left'})  => {
    M.Dropdown.init( document.querySelectorAll('.dropdown-trigger'), options)
}

const initModal = () => {
	let modals = document.querySelectorAll('.modal')
	M.Modal.init(modals, {})
}

const initFormSelect = () => {
    let elems = document.querySelectorAll('select');
    return M.FormSelect.init(elems, {})
}

const initTooltip = () => {
	let elems = document.querySelectorAll('.tooltipped')
	elems.forEach(el => {
		let instance = M.Tooltip.init(el, {
			exitDelay: 0,
			enterDleay:0,
			margin: 2,
			inDuration: 100,
			outDuration: 100,
			transitionMovement: 5
		})
		el.onclick = function(){
			instance.close()
		}
	})
}

const initRangeInput =  () => {
	M.Range.init(document.querySelectorAll("input[type=range]"))
}

const initDragMoveDialog = () => {
	interact('.dialog').draggable({
		allowFrom: 'h1',
		onmove: (event) => {
			const target = event.target
			const x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
			const y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy
			target.style.webkitTransform = 'translate(' + x + 'px, ' + y + 'px)'
			target.style.transform = 'translate(' + x + 'px, ' + y + 'px)'
			target.setAttribute('data-x', x)
			target.setAttribute('data-y', y)
		}
	})
}

const initResizeDragLegend = () => {
	interact('.resize-drag-legend').resizable({
		edges: { left: false, right: true, bottom: false, top: false },
		modifiers: [
			interact.modifiers.restrictSize({ 
				min: { width: 270 }, 
				max: { width: 500 } 
			})
		],
		inertia: true
	})
	.on('resizemove', function (event) {
		const target = event.target
		let x = (parseFloat(target.getAttribute('data-x')) || 0)
		let y = (parseFloat(target.getAttribute('data-y')) || 0)
		target.style.width = event.rect.width + 'px'
		target.style.height = event.rect.height + 'px'
		x += event.deltaRect.left
		y += event.deltaRect.top
		target.style.webkitTransform = 'translate(' + x + 'px,' + y + 'px)'
		target.style.transform = 'translate(' + x + 'px,' + y + 'px)'
		target.setAttribute('data-x', x)
		target.setAttribute('data-y', y)
	})	
}

const initCharacterCounter = () => {
	let elems = document.querySelectorAll('[data-length]')
	M.CharacterCounter.init(elems)
}

const initAutocomplete = (elem, options) => {
	return M.Autocomplete.init(elem, options)
}

const initCustomScrollBar = (elem, options = {}) => {
	OverlayScrollbars(elem, options)
}

export {initDropdown, initModal, initFormSelect, initRangeInput, initTooltip, 
		initDragMoveDialog, initResizeDragLegend, initCharacterCounter, initAutocomplete,
		initCustomScrollBar
}