import { writable } from 'svelte/store'

export const manager = writable()
export const selected = writable(null)

export const current = writable(null)
export const dragged = writable()
