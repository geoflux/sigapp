import { gis } from 'app/stores.js'
import { manager } from 'app/gis/stores.js'
import Sortable from 'sortablejs'    

let $manager
manager.subscribe(store => $manager = store )

async function getGis() {
    const response = await fetch(`API_URL/gis`)
    const json = await response.json()
    manager.update(store => store = json)
    return $manager
}

async function getFolder(id) {
    const response = await fetch(`API_URL/gis/folders/${id}`)
    const json = await response.json()
    return json
}

function updateStore () {
    gis.update(store => store = $manager)
}

async function createFolder(folder, parent = null) {
    const response = await fetch(`API_URL/gis/folders`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        body: JSON.stringify(folder)
    })
    const json = await response.json()
    json.layers = []
    json.subfolders = []
    if (parent !== null) {
        const updated = findFolder(parent.id)
        console.log(json)
        updated.subfolders.push(json)
    }
    else {
        $manager.catalog.push(json)
    }
    manager.update(store => store = $manager)
}

async function updateFolder(folder) {
    const response = await fetch(`API_URL/gis/folders/${folder.id}`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        body: JSON.stringify(folder)
    })
    await response.json()
    let updated = findFolder(folder.id)
    updated = folder
    manager.update(store => store = $manager)
}

async function deleteFolder(folder) {
    const response = await fetch(`API_URL/gis/folders/${folder.id}`, {
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        method: 'DELETE'
    })
    const json = await response.json()
    const parent = findFolder(folder.parent_id)
    console.log(parent)
    parent.subfolders = parent.subfolders.filter(items => items.id !== folder.id)
    $manager.tempdir = json
    console.log(json)
    manager.update(store => store = $manager)
}

async function sort(containerClass) {
    const element = document.querySelector('.' + containerClass)
    new Sortable(element, {
        preventOnFilter: false,
        // handle: '.handle',
        animation: 300,
        filter: '.ignore-sortable',
        easing: "cubic-bezier(1, 0, 0, 1)",
        ghostClass: "z-depth-3",
        setData: (dataTransfer, dragEl) => {
            dataTransfer.setData('Text', JSON.stringify({
                id: parseInt(dragEl.dataset.id), 
                type: 'folder'
            }))
        },
        onEnd: async (e) => {
            const newOrder = []
            const order = Array.from( e.to.querySelectorAll('header') )
            order.forEach( el => newOrder.push( parseInt(el.dataset.id)) )
            // $portals.sort((a, b) => (newOrder.indexOf(a.id) - newOrder.indexOf(b.id)) )
            const response = await fetch(`API_URL/gis/folders/reorder/from-array`, {
                method: 'PUT',
                headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
                body: JSON.stringify(newOrder)
            })
            const json = await response.text()
            console.log(json)
        },
    })    
}

async function getFolderPaths() {
    const response = await fetch(`API_URL/gis/folders`)
    return await response.json()
}

async function searchLayer(value) {
    if( value.length > 1) {
        const response = await fetch(`API_URL/gis/search/${value}`)
        return await response.json()   
    }
    return []
}

export { getGis, getFolder, updateStore, createFolder, sort, updateFolder, deleteFolder, getFolderPaths, searchLayer }




function findFolder(id) {
    if (id === 0) {
        return $manager.tempdir
    }
    let folder
    $manager.catalog.forEach(item => {
        if (item.id === id) {
            folder = item
        }
        else {
            folder = findSubfolder(item, id)
        }
    })
    return folder
}

function findSubfolder(folder, searched_id) {
    folder.subfolders.forEach(item => {
        if (item.id === searched_id) {
            return item
        }
        findSubfolder(item, searched_id)
    })
}