import { displayed, canvas } from 'app/stores.js'

let $displayed, $canvas
displayed.subscribe(value => { $displayed = value })
canvas.subscribe(value => { $canvas = value })

const displayPortals = () => { 
    $displayed.portals.active = !$displayed.portals.active
    if($displayed.portals.active) {
        $displayed.header = {nav: false, openPortals: true, openManager: false, location: false}
        $displayed.nav.nav = false
        $displayed.portals.portals = true
    } else {
        $displayed.header = {nav: true, openPortals: true, openManager: true, location: true, portals: false}         
        $displayed.nav.nav = true
        $displayed.portals.portals = false
    }
    displayed.update(store => store = $displayed)
}

const updateSlider = (value) => {
    $displayed.nav.slider = value
    $displayed.canvas.left = !value ? 0 : '270px'
    $displayed.footer.left = !value ? '45px' : '315px'
    displayed.update(store => store = $displayed)
}

const displayManager = (active) => {
    if(active) {
        $displayed.header = {nav: false, openPortals: false, openManager: true, location: false, portals: false}
        $displayed.manager = true
    } else {
        $displayed.header = {nav: true, openPortals: true, openManager: true, location: true, portals: false}
        $displayed.manager = false
    }
    displayed.update(store => store = $displayed)
}

const showFooter = (uuid) => {
    $canvas.selectionLayerSource.getFeatures().forEach(feature => {
        if (feature.id_.indexOf('selectid') > -1) {
            $canvas.removeGeoJsonFeature(feature.id_)
        }
    })
    $displayed.footer = { datatable: true, size: '33.333vh', uuid: uuid }
    $displayed.canvas.bottom = '33.333vh'
    displayed.update(store => store = $displayed)    
}

const hideFooter = () => {
    $displayed.footer = { datatable: false, size: 0, layer: false }
    $displayed.canvas.bottom = 0
    displayed.update(store => store = $displayed)
    $canvas.selectionLayerSource.getFeatures().forEach(feature => {
        if (feature.id_.indexOf('selectid') > -1) {
            $canvas.removeGeoJsonFeature(feature.id_)
        }
    })
    return '33.333vh'
}

const setPrintView = () => {
    if ($displayed.printer.active) {
        return unsetPrintView()
    }
    const mapElement = document.getElementById('map')
    const width = window.innerWidth - (45 + 270 + 20)
    const height = window.innerHeight - (45 + 40)
    const coef_height = ((height * 100) / 2339) / 100
    const coef_width = ((width * 100) / 3307) / 100
    const coef = (coef_height > coef_width) ? coef_width : coef_height
    mapElement.classList.add('z-depth-2')
    mapElement.style.height = `${2339 * coef}px`
    mapElement.style.width = `${3307 * coef}px`
    mapElement.style.padding = '0.4%'
    mapElement.style.background = '#fff'
    mapElement.style.margin = '20px auto'
    const extent = $canvas.getExtent()
    $canvas.setZoomFactor(1.2)
    $canvas.fit(extent)
    $canvas.map.updateSize()
    $displayed.printer.active = true
    updateSlider('printer')
    return
}

const unsetPrintView = () => {
    const mapElement = document.getElementById('map')
    mapElement.classList.remove('z-depth-2')
    mapElement.style.height = '100%'
    mapElement.style.width = '100%'
    mapElement.style.padding = '0'
    mapElement.style.background = 'transparent'
    mapElement.style.margin = '0'
    $displayed.printer.active = false
    updateSlider('layers')
    const extent = $canvas.getExtent()
    $canvas.setZoomFactor(2)
    $canvas.fit(extent)
    $canvas.map.getView().setZoom( $canvas.map.getView().getZoom() + 1 )
    $canvas.map.updateSize()
}


export { displayPortals, updateSlider, displayManager, showFooter, hideFooter, setPrintView, unsetPrintView }