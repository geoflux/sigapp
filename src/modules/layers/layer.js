import { canvas, maplayers, layer } from 'app/stores.js'
import ImageLayer from 'ol/layer/Image'
import ImageWMS from 'ol/source/ImageWMS'

let $canvas, $maplayers
canvas.subscribe(store => $canvas = store)
maplayers.subscribe(store => $maplayers = store)

export default class Layer {
	constructor(layer) {
        for ( const [key, value] of Object.entries(layer) ) {
            this[key] = value
        }
		this.sort = $maplayers.length + 1
		this.opacity = (typeof layer.opacity === 'undefined') ? 1 : layer.opacity
		this.visible = (typeof layer.visible === 'undefined') ? true : layer.visible
		this.filter = (typeof layer.filter === 'undefined') ? false : layer.filter
		this.uuid = Math.random().toString(36).substr(2, 9)
		this.toggled = true
		this.extent = false
		this.ol = new ImageLayer({
			opacity: this.opacity,
			visible: this.visible,
			zIndex: this.sort,
			source: new ImageWMS({
				url: 'QGIS_SERVER_URL',
				params: {
					'MAP' : layer.map,
					'LAYERS': layer.name,
					'FORMAT':'image/png',
					'VERSION': '1.3.1',
				},
				serverType: 'qgis',
			})
		})
		this.ol.set('uuid', this.uuid)
		if ( this.filter ) {
			this.setFilter(this.filter)
		}
		$canvas.addToLayerGroup('Layers', this.ol)
	}

    remove() {
		$canvas.removeFromLayerGroup('Layers', this.ol)
    }

	setVisible() {
        this.visible = !this.visible
        this.ol.setVisible(this.visible)
		$canvas.map.updateSize()
		this.updateMaplayers()
    }

	setOpacity(value) {
        this.opacity = parseFloat(value)
        this.ol.setOpacity(this.opacity)
        $canvas.map.updateSize()
	}

	setFilter(query) {
		if(query != null && query.length > 8){
			this.ol.getSource().updateParams({
				'FILTER' : this.name + ':' + query
			})
			this.filter = query
		} else {
			this.ol.getSource().updateParams({
				'FILTER' : ''
			})
			this.filter = false
		}
		$canvas.map.updateSize()
		this.extent = false
		this.updateMaplayers()
	}

	async getDatatable(fromExtent = false) {
		const settings = await this.getColumns()
		let response
		if (fromExtent) {
			const extent = $canvas.getExtent()
			response = await fetch(`API_URL/layers/${this.id}/datatable/${extent.xmin}/${extent.ymin}/${extent.xmax}/${extent.ymax}/${extent.srid}?filter=${this.filter}`)
		} else {
			response = await fetch(`API_URL/layers/${this.id}/datatable?filter=${this.filter}`)
		}
		const rows  = await response.json()
		const columns = []
		Object.entries(rows[0]).forEach(cell => {
			const option = settings.find(option => option.alias === cell[0])
			if (option) {
				columns.push({ value: cell[0], numeric: option.numeric, unit: option.unit })
			}
			else {
				columns.push({ value: cell[0], numeric: false, unit: '' })
			}
		})
		return {
			columns: columns,
			rows: rows,
		}
	}

	async fit() {
		if (!this.extent) {
			const response = await fetch(`API_URL/layers/${this.id}/extent?filter=${this.filter}`)
			const json  = await response.json()
			this.extent = json
		}
		$canvas.fit( this.extent )
	}

	async fitEntity(identifier) {
		const response = await fetch(`API_URL/layers/${this.id}/extent/${identifier}`)
		const json  = await response.json()
		$canvas.fit( json )
	}

	async getColumns() {
		const response = await fetch(`API_URL/layers/${this.id}/columns`)
		const json = await response.json()
		return json
	}

	async getDistinctValues(attribute) {
		const response = await fetch(`API_URL/layers/${this.id}/values/${attribute}`)
		const json = await response.json()
		return json
	}

	async getFeatureInfo(identifier) {
		const response = await fetch(`API_URL/layers/${this.id}/featureinfo/${identifier}`)
		const json = await response.json()
		return json
	}

	async getFeatureInfoFromCoordinates(x, y ,zoom) {
		const response = await fetch(`API_URL/layers/${this.id}/featureinfo/${x}/${y}/${zoom}?filter=${this.filter}`)
		const json = await response.json()
		return json
	}

	makeAvailableInStore(uuid) {
		let i = $maplayers.findIndex((obj => obj.uuid === uuid))
		layer.update(object => object = $maplayers[i])
	}

	updateMaplayers() {
		let i = $maplayers.findIndex((obj => obj.uuid === this.uuid))
		$maplayers[i] = this
		maplayers.update(store => store = $maplayers)
		layer.update(store => store = this)
	}
}