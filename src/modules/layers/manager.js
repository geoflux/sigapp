
async function updateLayerFolder(layer, folder) {
    const response = await fetch(`API_URL/layers/${layer.id}/folder/${folder.id}`, {
        method: 'PUT'
    })
    await response.json()
}

async function updateLayer(layer) {
    const response = await fetch(`API_URL/layers/${layer.id}`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        body: JSON.stringify(layer)           
    })
    const json = await response.json()
    // selected.update(store => store = json)
}

async function updateLayerPreviews(layer) {
    const response = await fetch(`API_URL/layers/${layer.id}/previews`, {
        headers: {'Accept' : 'application/json' },
        method: 'PUT'  
    })
    const json = await response.json()
    console.log(json)
    return json
}

async function updateLayerExtent(layer) {
    const response = await fetch(`API_URL/layers/${layer.id}/extent`, {
        headers: {'Accept' : 'application/json' },
        method: 'PUT'  
    })
    const json = await response.json()
    console.log(json)
    return json
}

async function getLayerColumns(layer) {
    const response = await fetch(`API_URL/layers/${layer.id}/columns`)
    return await response.json()   
}

async function updateLayerColumns(layer, columns) {
    const response = await fetch(`API_URL/layers/${layer.id}/columns`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        body: JSON.stringify(columns)          
    })
    return await response.json()   
}

async function extractLayer(layer, format, extent = []) {
    const response = await fetch(`API_URL/layers/${layer.id}/extract/${format}?filter=${layer.filter}`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json', 'Accept' : 'application/json' },
        body: JSON.stringify({extent: extent}) 
    })
    return await response.json()
}

async function deleteLayer(layer) {
    const response = await fetch(`API_URL/layers/${layer.id}`, {
        headers: {'Accept' : 'application/json' },
        method: 'DELETE'  
    })
    await response.json()   
}

async function updateLayerProject(layer, projectName) {
    await fetch(`API_URL/layers/${layer.id}/project/${projectName}`, {
        headers: {'Accept' : 'application/json' },
        method: 'PUT'
    })
}

async function replaceLayer(layer, replacement) {
    const response = await fetch(`API_URL/layers/${layer.id}/replace/${replacement}`, {
        headers: {'Accept' : 'application/json' },
        method: 'PUT'
    })
    const json = await response.json()
    console.log(json)
    return json
}

export { updateLayerFolder, updateLayer, updateLayerPreviews, updateLayerExtent, 
    getLayerColumns, updateLayerColumns, extractLayer, updateLayerProject, replaceLayer, deleteLayer }