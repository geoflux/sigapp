import { canvas } from 'app/stores.js'
import { DataTable as SimpleDatatable } from 'simple-datatables/dist'

let $canvas
canvas.subscribe(store => $canvas = store)

export default class Datatable {
	constructor(layer) {
        this.layer = layer
        this.init()
	}

    init() {
        this.table = new SimpleDatatable( document.querySelector("#simple-datatable"), {
            searchable: true,
            fixedHeight: true,
            scrollY: "100px",
            fixedColumns: true,
            sortable: true,
            paging: true,
            columns: [
                { select: 0, sortable: false },
                { select: 1, sort: "asc" },
            ],
            labels: {
                placeholder: "Rechercher...",
                perPage: "{select} entrée par page",
                noRows: "Aucune entité géographique",
                info: "Lignes {start} à {end} sur {rows}",
            },
            layout: {
                top: "{search}",
                bottom: "{info}{pager}"
            },
            perPage: 100,
        })
        document.querySelector(".dataTable-input").classList.add('browser-default')
        this.table.on('datatable.page', () => { this.addEvents() })
        this.table.on('datatable.sort', () => { this.addEvents() })
        this.table.on('datatable.search', () => { this.addEvents() })
    }

    updateSize(coefficient) {
        const container = document.querySelector(".dataTable-container")
        let height = (window.innerHeight * coefficient) - 90
        container.style.maxHeight = `${height}px`
        container.style.minHeight = `${height}px`
    }

    addEvents() {
        const waitForTable = setInterval( () => {
            if ( this.table.initialized ) {
                document.querySelectorAll('td .select-entity').forEach(item => {
                    item.addEventListener('input', (e) => {
                        this.selectEntity(e)
                    })
                })
                document.querySelectorAll('td .focus-entity').forEach(item => {
                    item.addEventListener('click', (e) => {
                        this.focusEntity(e)
                    })
                })
                clearInterval(waitForTable)
            }
        }, 100)
    }

    selectEntity(e) {
        const row = e.target.parentNode.parentNode.parentNode
        const identifier = e.target.dataset.id
        const featureId = `${this.layer.uuid}selectid${identifier}`
        if(e.target.checked) { 
            row.classList.add('active')
            this.layer.getFeatureInfo(identifier).then(geojson => {
                if(geojson) {
                    $canvas.addGeoJsonFeature(geojson, featureId)
                }
            })
        } else {
            row.classList.remove('active')
            $canvas.removeGeoJsonFeature(featureId)
        }
    }

    focusEntity(e) {
        const identifier = e.target.dataset.id
        this.layer.fitEntity(identifier)
    }
}