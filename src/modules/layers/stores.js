import { writable } from 'svelte/store'

export const current = writable(null)
export const selected = writable(null)
export const columns = writable(null)
export const over = writable(false)