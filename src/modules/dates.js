export const getIntlDateTimeFormat = (dateString) => {
    const date = new Date(dateString);
    return new Intl.DateTimeFormat('fr-FR', {
        year: 'numeric',
        month: 'short',
        day: 'numeric'
    }).format(date)
}

export const getIntlRelativeTimeFormat = (dateString) => {
    let format
    const rtf = new Intl.RelativeTimeFormat('fr-FR', { numeric: 'auto' })
    let fromNow = parseInt((new Date(dateString) - new Date()) / (24 * 60 * 60 * 1000))
    if (fromNow < -31) {
        fromNow = parseInt(fromNow / 31)
        format = 'month'
    }
    else if (fromNow < -365) {
        fromNow = parseInt(fromNow / 365)
        format = 'year'
    }
    else {
        format = 'day'
    }
    return rtf.format(fromNow, format)
}