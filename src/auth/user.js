const getRemoteUserAccount = async () => {
    const response = await fetch(`AUTH_API_URL/login/remote-user`, {
        headers: { 'Accept' : 'application/json' },
        method: 'GET'
    })
    if (response.status === 500) {
        console.log(response.json())
        window.location.href = $next
    }
    if (response.status === 412) {
        return false
    }
    const json = await response.json()
    document.cookie = `geolab_token=${json.token};path=/`
    window.location.replace($next)
    return true
}

const login = async (form) => {
    const response = await fetch(`API_URL/login`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json' },
        body: JSON.stringify(form)
    })
    if (response.status !== 200) {
        return 'Identifiant ou mot de passe incorrect'
    }
    const json = await response.json()
    saveToken(json.token)
    window.location.replace($next)
}

const registerUser = async (form) => {
    const response = await fetch(`API_URL/register`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json' },
        body: JSON.stringify(form)
    })
    if (response.status === 412) {
        return await response.json()
    }
    return null
}

const saveToken = (token) => {
    const date = new Date()
    date.setTime( date.getTime() + (2 * 24 * 60 * 60 * 1000) )
    document.cookie = `geolab_token=${token};expires=${date.toUTCString()};path=/`
}

const logout = async () => {
    document.cookie = `geolab_token=null;path=/`
}

export { getRemoteUserAccount, login, saveToken, registerUser, logout }