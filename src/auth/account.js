import { user } from 'auth/stores.js'

async function getAccount() {
    let auth = false
    try {
        const response = await fetch(`AUTH_API_URL/account`)
        if (response.ok) {
            auth = true
            const json = await response.json()
            user.update(store => store = json)
            saveToken(json.jwt)
            return
        }
    } finally {
        if (!auth) {
            await isGuestAllowed()
        }
    }
}
 

async function isGuestAllowed() {
    if (`GUESTS` === 'true') {
        const response = await fetch(`AUTH_API_URL/account/guest`)
        const json = await response.json()
        user.update(store => store = json)
        saveToken(json.jwt)
        return
    } else {
        return window.location.replace(`AUTH_URL?next=${ encodeURI(window.location.href) }`)
    }
}

async function updateAccount(account) {
    const response = await fetch(`AUTH_API_URL/account`, {
        method: 'PUT',
        headers: {'Content-Type' : 'application/json' },
        body: JSON.stringify(account)
    })
    if (response.status === 412) {
        return await response.json()
    }
    const json = await response.json()
    user.update(store => store = json)
    return []
}

async function updateAccountPassword(form) {
    const response = await fetch(`AUTH_API_URL/account/password`, {
        method: 'POST',
        headers: {'Content-Type' : 'application/json' },
        body: JSON.stringify(form)
    })
    if (response.status === 412) {
        return await response.json()
    }
    const json = await response.json()
    user.update(store => store = json)
    return []
}

async function deleteAccount() {
    const response = await fetch(`AUTH_API_URL/account`, {
        method: 'DELETE'
    })
    if (response.status === 200) {
        return window.location.replace(`AUTH_URL`)
    }
    const json = await response.text()
    console.log(json)
    return
}

const saveToken = (token) => {
    const date = new Date()
    date.setTime( date.getTime() + (2 * 24 * 60 * 60 * 1000) )
    document.cookie = `geolab_token=${token};expires=${date.toUTCString()};path=/`
}

export { getAccount, updateAccount, updateAccountPassword, deleteAccount }