import { writable } from 'svelte/store';

export const user = writable({})
export const manager = writable(false)
export const page = writable('show')