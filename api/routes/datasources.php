<?php
use \Sigapp\Datasources\DatasourcesController;

/**
 * @OA\Get(
 *     path="/datasources",
 *     tags={"Datasources"},
 *     summary="Return datasources",
 *     @OA\Response(response="200", description="",)
 * )
 */
$app->get('/datasources', DatasourcesController::class . ":index")->add($isAdmin); 

// 
// Delete(
//      path="/datasources",
//      tags={"Datasources"},
//      summary="Delete all data sources that are not used by any layer",
//      Response( response="200", description="", )
// )
// 
// $app->delete('/datasources', DatasourcesController::class . ":destroy")->add($isAdmin); 