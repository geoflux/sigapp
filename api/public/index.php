﻿<?php
require __DIR__ . "/../config/config.php";
require __DIR__ . "/../config/middlewares.php";

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', 'http://localhost:5000')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

require "./test.php";

/**
 * @OA\Info(title="Sigapp API", version="")
 * @OA\Server(url=API_URL, description="localhost")
 * @OA\Server(url="https://geoflux.io/sigapp.api/", description="geoflux.io")
 */

/**
 * @OA\Tag(name="Settings", description="General application settings")
 */
require __DIR__ . "/../routes/settings.php";

/**
 * @OA\Tag(name="Qgis Projects")
 */
require __DIR__ . "/../routes/qgis_projects.php";

/**
 * @OA\Tag(name="Gis", description="Set of thematic directories to organize the layers imported into the application")
 */ 
require __DIR__ . "/../routes/gis.php";

/**
 * @OA\Tag(name="Portals", description="A portal is a collection of saved maps")
 */
require __DIR__ . "/../routes/portals.php";

/** 
 * @OA\Tag(name="Maps", description="Maps contain layers and basemap with parameters")
 */
require __DIR__ . "/../routes/maps.php";

/** 
 * @OA\Tag(name="Layers")
 */ 
require __DIR__ . "/../routes/layers.php";

/** 
 * @OA\Tag(name="Basemaps")
 */ 
require __DIR__ . "/../routes/basemaps.php";

/** 
 * @OA\Tag(name="Datasources", description="Registered connections to databases")
 */
require __DIR__ . "/../routes/datasources.php";

/**
 * @OA\Tag(name="Permalinks", description="Permanent link of a map defined by a unique token")
 */ 
require __DIR__ . "/../routes/permalinks.php";

$app->run();