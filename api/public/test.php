<?php

$app->get('/test/datastore', function ($request, $response){
    $qgis = new \Core\Qgis\QgisAdapter(PATH_TO_MAPS . '/test-dump/test-qgs.qgs');
    $data = $qgis->getLayersDump()[0];
    $store = \Core\Datastore\DatastoreManager::dump($data);
    // $drop = \Core\Datastore\DatastoreManager::drop($data['tablename']);
    return $response
        // ->withHeader('Content-Type', 'application/json')
        // ->withJson( $adapter->getLayers()->getLayersByProvider('ogr') );
        ->write(print_r( $store ));
});

$app->get('/test/datastore/drop', function ($request, $response){
    $tablename = 'departements_5e64ed0de4b4f';
    $drop = \Core\Datastore\DatastoreManager::drop($tablename);
    return $response
        // ->withHeader('Content-Type', 'application/json')
        // ->withJson( $adapter->getLayers()->getLayersByProvider('ogr') );
        ->write(print_r( $drop ));
});


$app->get('/test/file', function ($request, $response){
    $file = new \Core\File\FileModel(PATH_TO_MAPS . 'TEMP_5e5e1c3fb5a22/zip_test_with_shapes_5e5e1c3fe155a.qgs');
    $storage = $file->store();
    return $response
        // ->withHeader('Content-Type', 'application/json')
        // ->withJson( $adapter->getLayers()->getLayersByProvider('ogr') );
        ->write(print_r( $storage ));
});

$app->get('/test/string', function ($request, $response){
    $string = './departements.geojson|layername=departements';
    $strpos = strpos($string, 'layername');
    return $response
        // ->withHeader('Content-Type', 'application/json')
        // ->withJson( $adapter->getLayers()->getLayersByProvider('ogr') );
        ->write(var_dump( $strpos ));
});

$app->get('/test/ogr', function ($request, $response){
    $ogr = new \Sigapp\Layers\IO\Ogr( 87, 'geojson', '', [] );
    return $response
        // ->withHeader('Content-Type', 'application/json')
        // ->withJson( $adapter->getLayers()->getLayersByProvider('ogr') );
        ->write(print_r( $ogr->process() ));
});

$app->get('/test/io', function ($request, $response){
    $io = new \Sigapp\Layers\LayersIO(101, 'geojson', '', []);
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withJson( $io->extract() );
        // ->write(print_r( $io->extract() ));
});

$app->get('/test/extent', function ($request, $response){
    $extent = [
        'srid'  => 2154,
        'xmin'  => 743342.52241658431012183 ,
        'ymin'  => 6548361.20002481993287802,
        'xmax'  => 889735.19383138220291585 ,
        'ymax'  => 6672193.08610744681209326,
    ];
    $adapter = new \Core\Adapters\ExtentAdapter($extent);
    return $response
        // ->withHeader('Content-Type', 'application/json')
        // ->withJson( $adapter->getPerimeter() );
        ->write(print_r( $adapter->expand(0.09) ));
});

$app->get('/test/qgis', function ($request, $response){
    
    $qgis = new \Core\Qgis\QgisModel( PATH_TO_MAPS . 'covage.qgs');
    $layer = $qgis->getLayerByName('covage_commune_convention_afe412b9_74d0_4358_a24f_fa7ba04276f1');
    return $response
        ->write(print_r( $layer->getExtent() ));
});