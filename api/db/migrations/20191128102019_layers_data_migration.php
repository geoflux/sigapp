<?php
use \Database\Migrations\Migration;
use \Illuminate\Database\Schema\Blueprint;

class LayersDataMigration extends Migration
{
    public function up()  {
        $this->schema->create('layers_data', function(Blueprint $table){
            $table->integer('layer_id')->unique();
            $table->string('name');
            $table->string('tablename');
            $table->string('displayfield');
            $table->string('geomcolumn');
            $table->string('geomtype'); // POLYGON, LINESTRING, POINT, MULTIPLOLYGON...
            $table->integer('srid');
            $table->json('extent');
            $table->text('sql')->nullable();
            $table->json('columns');
            $table->text('datatable')->nullable();
            $table->string('project_name');
            $table->integer('datasource_id');
            $table->timestamps();
        });
    }
    public function down()  {
        $this->schema->drop('layers_data');
    }
}
