<?php
namespace Database;

use \PDO;
use \Spatialite\SPL;

class Datastore
{
    public static function mount(): void
    {
        if ( getenv('ENVIRONMENT') === 'development' && !file_exists(__DIR__ . '/datastore.db') ) {
            SPL::CreateNewEmptyDB( __DIR__ . '/datastore.db' );
        }
        elseif ( getenv('ENVIRONMENT') === 'production' && !file_exists(__DIR__ . '/datastore.mounted') ) {
            self::connect()->exec('
                CREATE EXTENSION postgis;
                CREATE SCHEMA datastore;
            ');
            fopen(__DIR__ . '/datastore.mounted', 'w');
        }
    }

    public static function connect()
    {
        if ( getenv('ENVIRONMENT') === 'development' ) {
            return new SPL( __DIR__ . '/datastore.db' );
        }
        elseif ( getenv('ENVIRONMENT') === 'production' ) {
            return new PDO('pgsql:host='. getenv('DB_HOST') .';port='. getenv('DB_PORT') .';dbname='. getenv('DB_NAME') .';user='. getenv('DB_USER') .';password='. getenv('DB_PASSWORD') );
        }       
    }

    public static function getParams()
    {
        if ( getenv('ENVIRONMENT') === 'development' ) {
            return [
                'driver'    => 'SQLite',
                'dbname'    => __DIR__ . '/datastore.db',
                'provider'  => 'spatialite',
                'database'  => __DIR__ . '/datastore.db',
            ];
        }
        elseif ( getenv('ENVIRONMENT') === 'production' ) {
            return [
                'driver'    => 'PostgreSQL',
                'host'      => getenv('DB_HOST'),
                'port'      => getenv('DB_PORT'),
                'dbname'    => getenv('DB_NAME'),
                'user'      => getenv('DB_USER'),
                'password'  => getenv('DB_PASSWORD'),
                'provider'  => 'postgres',
                'database'  => 'dbname=\''. getenv('DB_NAME') .'\' host=\''. getenv('DB_HOST') .'\' port=\''. getenv('DB_PORT') .'\' user=\''. getenv('DB_USER') .'\' password=\''. getenv('DB_PASSWORD') .'\''
            ];
        }
    }
}