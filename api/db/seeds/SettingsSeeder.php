<?php
use Database\Database;
use Phinx\Seed\AbstractSeed;
use \Sigapp\Settings\SettingsModel;

class SettingsSeeder extends AbstractSeed
{
    public function run()
    {
        Database::init();
        SettingsModel::create([
            'name'  => 'application',
            'value' => [
                'name' => 'Sigapp',
                'logo'  => API_URL . 'images/logo.svg',
            ],
        ]);
    }
}