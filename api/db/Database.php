<?php
namespace Database;

use Illuminate\Database\Capsule\Manager as Capsule;

class Database
{
    public static function init()
    {
        $capsule = new Capsule;
        $capsule->addConnection( self::connection() );
        $capsule->bootEloquent();
        $capsule->setAsGlobal();
        return [
            'schema' => $capsule->schema(),
            'db'     => $capsule->getConnection(),
        ];
    }

	public static function connection()  {
        if ( getenv('ENVIRONMENT') === 'development' ) {
            $db = __DIR__ . '/sigapp.db';
            if (!file_exists($db)) fopen($db, 'w');
            return [
                'driver'    => 'sqlite',
                'database'  => $db,
                'charset'   => 'utf8',
            ];
        }
        elseif ( getenv('ENVIRONMENT') === 'production' ) {
            return [
                'driver'    => 'pgsql',
                'host'      => getenv('DB_HOST'),
                'port'      => getenv('DB_PORT'),
                'database'  => getenv('DB_NAME'),
                'username'  => getenv('DB_USER'),
                'password'  => getenv('DB_PASSWORD'),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => ''
            ];
        }
        return null;
    }
}