<?php
namespace Core\Qgis;

class QmlModel
{
    protected $qml;

    public function __construct()
    {
        $this->qml = simplexml_load_file( PATH_TO_MAPS . 'template.style.qml' );
    }

    public function copyLayerStyle(\SimpleXMLElement $layer): QmlModel
    {
        $this
            ->appendChild($layer->flags)
            ->appendChild($layer->{'renderer-v2'})
            ->appendChild($layer->customproperties)
            ->appendChild($layer->blendMode)
            ->appendChild($layer->featureBlendMode)
            ->appendChild($layer->layerOpacity)
            ->appendChild($layer->SingleCategoryDiagramRenderer)
            ->appendChild($layer->DiagramLayerSettings)
            ->appendChild($layer->geometryOptions)
            ->appendChild($layer->fieldConfiguration)
            ->appendChild($layer->aliases)
            ->appendChild($layer->excludeAttributesWMS)
            ->appendChild($layer->excludeAttributesWFS)
            ->appendChild($layer->defaults)
            ->appendChild($layer->constraints)
            ->appendChild($layer->constraintExpressions)
            ->appendChild($layer->expressionfields)
            ->appendChild($layer->attributeactions)
            ->appendChild($layer->attributetableconfig)
            ->appendChild($layer->conditionalstyles)
            ->appendChild($layer->editform)
            ->appendChild($layer->editforminit)
            ->appendChild($layer->editforminitfilepath)
            ->appendChild($layer->editforminitcode);
        return $this;
    }

    public function save(string $file): string
    {
        $this->qml->asXml( $file );
        return $file;
    }

    private function appendChild(\SimpleXmlElement $xmlNode): QmlModel
    {
        $child = dom_import_simplexml($xmlNode);
        $parent = dom_import_simplexml($this->qml);
        $parent->appendChild( $parent->ownerDocument->importNode($child, true) );
        return $this;  
    }
}