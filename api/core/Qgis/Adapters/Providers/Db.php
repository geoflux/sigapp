<?php
namespace Core\Qgis\Adapters\Providers;

use \Core\Qgis\Parsers\LayerParser;

class Db
{
    private $layer;
    private $project_name;
    private $projectfile;

    public function __construct(LayerParser $layer, string $projectfile)
    {
        $this->layer = $layer;
        $this->project_name =  pathinfo( $projectfile, PATHINFO_FILENAME );
        $this->projectfile = $projectfile;
    }

    public function getLayer(): array
    {
        return [
            'name'          => $this->layer->getName(),
            'title'         => $this->layer->getTitle(),
            'abstract'      => $this->layer->getAbstract(),
            'source'        => $this->layer->getSource(),
            'project_name'  => $this->project_name,
            'map'           => $this->projectfile,
            'properties'    => $this->getProperties(),
            'datasource'    => $this->getDatasource(),
        ];        
    }

    private function getProperties(): array
    {
        return [
            'name'          => $this->layer->getName(),
            'tablename'     => $this->layer->getTablename(),
            'displayfield'  => $this->layer->getDisplayfield(),
            'geomcolumn'    => $this->layer->getGeomcolumn(),
            'geomtype'      => $this->layer->getGeometryType(),
            'srid'          => $this->layer->getSrid(),
            'extent'        => $this->layer->getExtent(),
            'sql'           => $this->layer->getFilter(),
            'columns'       => $this->layer->getColumns(),
            'project_name'  => $this->project_name,
        ];
    }

    private function getDatasource(): array
    {
        $datasource = $this->layer->getDatasource();
        return [
            'provider'  => $this->layer->getProvider(),
            'host'      => key_exists('host', $datasource) ? $datasource['host'] : null,
            'port'      => key_exists('port', $datasource) ? $datasource['port'] : null,
            'dbname'    => $datasource['dbname'],
            'user'      => key_exists('user', $datasource) ? $datasource['user'] : null,
            'password'  => key_exists('password', $datasource) ? $datasource['password'] : null,
        ];
    }
}