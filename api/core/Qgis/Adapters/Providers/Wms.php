<?php
namespace Core\Qgis\Adapters\Providers;

use \Core\Qgis\Parsers\layerParser;

class Wms
{
    private $layer;
    private $project_name;
    private $projectfile;

    public function __construct(LayerParser $layer, string $projectfile)
    {
        $this->layer = $layer;
        $this->project_name =  pathinfo( $projectfile, PATHINFO_FILENAME );
    }

    public function getLayer(): array
    {
        $datasource = $this->layer->getDatasource();
        return [
            'name'          => $this->layer->getName(),
            'title'         => $this->layer->getTitle(),
            'attributions'  => $this->layer->getSource(),
            'service'       => key_exists('type', $datasource) ? 'TMS' : 'WMTS',
            'url'           => $datasource['url'],
            'layer'         => key_exists('layers', $datasource) ? $datasource['layers'] : null,
            'format'        => key_exists('format', $datasource) ? $datasource['format'] : null,
            'style'         => key_exists('styles', $datasource) ? $datasource['styles'] : null,
            'project_name'  => $this->project_name,
            'external_wms'  => $datasource['external_wms']
        ];
    }
}