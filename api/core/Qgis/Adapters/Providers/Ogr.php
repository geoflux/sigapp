<?php
namespace Core\Qgis\Adapters\Providers;

use \Core\Qgis\Parsers\LayerParser;

class Ogr
{
    private $layer;
    private $project_name;
    private $projectfile;

    public function __construct(LayerParser $layer, string $projectfile)
    {
        $this->layer = $layer;
        $this->project_name =  pathinfo( $projectfile, PATHINFO_FILENAME );
        $this->projectfile = $projectfile;
    }

    public function getLayer(): array
    {
        return [
            'name'          => $this->layer->getName(),
            'title'         => $this->layer->getTitle(),
            'abstract'      => $this->layer->getAbstract(),
            'source'        => $this->layer->getSource(),
            'project_name'  => $this->project_name,
            'map'           => $this->projectfile,
            'properties'    => $this->getProperties(),
        ];
    }

    private function getProperties(): array
    {
        $datasource = $this->layer->getDatasource();
        return [
            'name'          => $this->layer->getName(),
            'geomtype'      => $this->layer->getGeometryType(),
            'displayfield'  => $this->layer->getDisplayfield(),
            'srid'          => $this->layer->getSrid(),
            'extent'        => $this->layer->getExtent(),
            'columns'       => $this->layer->getColumns(),
            'file'          => pathinfo( $datasource['file'], PATHINFO_BASENAME ),
            'tablename'     => $datasource['tablename'],
            'geomcolumn'    => $datasource['geomcolumn'],
            'encoding'      => $datasource['encoding'],
            'extension'     => $datasource['extension'],
            'sql'           => null,
            'project_name'  => $this->project_name,
        ];
    }
}