<?php
namespace Core\Qgis\Adapters;

use \Core\Qgis\Adapters\Providers\{ Db, Ogr, Wms };

class LayersAdapter
{
    protected $projectfile;
    protected $layers;

    public function __construct(\Core\Qgis\QgisAdapter $qgis)
    {
        $this->projectfile = $qgis->getProjectfile();
        $this->layers = $qgis->getLayers();
    }

    public function all(): array
    {
        foreach ($this->layers as $layer) {
            $layers[] = $this->getLayer($layer);
        }
        $layers = isset($layers) ? $layers : [];
        return $layers;
    }

    public function filterByProviders(array $providers = ['postgres', 'spatialite']): array
    {
        foreach ($this->layers as $layer) {
            if ( in_array($layer->getProvider(), $providers) ) {
                $layers[] = $this->getLayer($layer);
            }
        }
        $layers = isset($layers) ? $layers : [];
        return $layers;
    }

    public function find(string $name): array
    {
        foreach ($this->layers as $layer) {
            if ( $layer->getName() === $name) {
                return $this->getLayer($layer);
            }
        }
        return null;
    }

    private function getLayer(\Core\Qgis\Parsers\LayerParser $layer): array
    {
        $provider = $layer->getProvider();
        if ( in_array($provider, ['spatialite', 'postgres', 'oracle']) ) {
            return ( new Db($layer, $this->projectfile) )->getLayer();
        }
        elseif ($provider === 'ogr') {
            return ( new Ogr($layer, $this->projectfile) )->getLayer();
        }
        elseif ($provider === 'wms') {
            return ( new Wms($layer, $this->projectfile) )->getLayer();
        }
        return null;
    }
}