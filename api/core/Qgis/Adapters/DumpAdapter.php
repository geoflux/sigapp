<?php
namespace Core\Qgis\Adapters;

use \File\FileModel;
use Symfony\Component\Finder\Finder;
use Cocur\Slugify\Slugify;


class DumpAdapter extends LayersAdapter
{
    protected $projectfile;
    protected $layers;
    private $dir;

    public function __construct(\Core\Qgis\QgisAdapter $qgis)
    {
        parent::__construct($qgis);
        $this->dir = $this->getDir();
    }

    public function getDumpSettings(): array
    {
        $slugify = new Slugify(['separator' => '_']);
        foreach ( $this->filterByProviders(['ogr', 'spatialite']) as $layer ) {
            $file = isset($layer['datasource']['dbname']) ? $layer['datasource']['dbname'] : $layer['properties']['file'];
            $layers[] = [
                'project_name'      => $layer['project_name'],
                'layer_name'        => $layer['name'],
                'tablename'         => uniqid( $slugify->slugify($layer['properties']['tablename']) . '_'),
                'tablename_origin'  => $layer['properties']['tablename'],
                'geomtype'          => strtoupper($layer['properties']['geomtype']),
                'key'               => $layer['properties']['displayfield'],
                'file'              => $this->getRealpath($file),
                'projectfile'       => $layer['map'],
            ];
        }
        $layers = isset($layers) ? $layers : [];
        return $layers;       
    }

    private function getDir(): string
    {
        return pathinfo( $this->projectfile, PATHINFO_DIRNAME ) . '/';
    }

    private function getRealPath(string $filename): ?string
    {
        $search = pathinfo( $filename, PATHINFO_BASENAME );
        $finder = new Finder();
        $finder->in( $this->dir );
        $finder->files()->name([$search]);
        foreach ($finder as $file) {
            $paths[] = $file->getRealPath();
        }
        $paths = isset($paths) ? $paths : null;
        return $paths[0];
    }
}