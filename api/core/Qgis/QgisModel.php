<?php
namespace Core\Qgis;

class QgisModel extends AbstractQgis
{
    private const NEW_PROJECT = PATH_TO_MAPS . 'template.project.qgs';
    protected $projectfile;
    private $qgis;

    public function __construct(string $projectfile)
    {
        $this->projectfile = $projectfile;
        $this->getSimpleXMLElement();
    }

    public static function createNewProject()
    {
        $projectfile = PATH_TO_MAPS . uniqid('NEW_PROJECT_') . '.qgs';
		copy( self::NEW_PROJECT, $projectfile );
		return new self($projectfile);
    }
}