<?php
namespace Core\Qgis\Parsers;

use \Core\Adapters\ExtentAdapter;

class CanvasParser
{
    private $canvas;

    public function __construct(\SimpleXMLElement $canvas)
    {
        $this->canvas = $canvas;
    }

    public function getSrid(): int
    {
        return (integer) $this->canvas->destinationsrs->spatialrefsys->srid[0];
    }

    public function setSrid(int $value): void
    {
        $this->canvas->destinationsrs->spatialrefsys->srid = $value;
    }

    public function getExtent(): array
    {
		$extent = (array) $this->canvas->extent;
		$extent['srid'] = $this->getSrid();
		return ( new ExtentAdapter($extent) )->transform(3857);
    }

    public function setExtent(array $extent): void
    {
        $extent = ( new ExtentAdapter($extent) )->transform( $this->getSrid() );
        $this->canvas->extent->xmin = $extent['xmin'];
        $this->canvas->extent->ymin = $extent['ymin'];
        $this->canvas->extent->xmax = $extent['xmax'];
        $this->canvas->extent->ymax = $extent['ymax'];       

    }

    public function getRotation(): float
    {
        return (float) $this->canvas->rotation;
    }

    public function setRotation(float $value): void
    {
        $this->canvas->rotation = $value;
    }
}