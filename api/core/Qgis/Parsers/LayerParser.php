<?php
namespace Core\Qgis\Parsers;

use \Core\Qgis\Parsers\Layer\{ Datasource, Extent, Columns };

class LayerParser
{
    protected $layer;
    private $datasource;

    public function __construct(\SimpleXMLElement $layer)
    {
        $this->layer = $layer;
    }

    public function getSimpleXMLElement(): \SimpleXMLElement
    {
        return $this->layer;
    }

    public function getName(): string
    {
        return (string) $this->layer->id;
    }

    public function getTitle(): string
    {
        return (string) $this->layer->layername;
    }

    public function setTitle(string $value): void
    {
        $this->layer->layername = $value;
    }

    public function getAbstract(): ?string
    {
        if ( isset($this->layer->abstract) ) {
            return (string) $this->layer->abstract;
        }
        return null;
    }

    public function setAbstract(string $value): void
    {
        if ( empty($this->layer->abstract) ) {
            $this->layer->addChild('abstract');
        }
        $this->layer->abstract = $value;
    }

    public function getSource(): ?string
    {
        if ( isset($this->layer->attribution) ) {
            return (string) $this->layer->attribution;
        }
        return null;
    }

    public function setSource(string $value): void
    {
        if ( empty($this->layer->attribution) ) {
            $this->layer->addChild('attribution');
        }
        $this->layer->attribution = $value;
    }

    public function getProvider(): string
    {
        return (string) $this->layer->provider[0];
    }

    public function getTablename(): string
    {
        $datasource = ( new Datasource( $this->getSimpleXMLElement() ) )->get();
        return $datasource['tablename'];
    }

    public function getGeomcolumn(): string
    {
        $datasource = ( new Datasource( $this->getSimpleXMLElement() ) )->get();
        return $datasource['geomcolumn'];
    }

    public function getFilter(): string
    {
        $datasource = ( new Datasource( $this->getSimpleXMLElement() ) )->get();
        return $datasource['sql'];
    }

    public function getDisplayfield(): string
    {
        $displayfield = (string) $this->layer->previewExpression;
        $displayfield = str_replace([
            'COALESCE', '(', '"', ')', ' ', '&lt;', '<', '>', ';', 'NULL', "'", ','
        ], '', $displayfield);
        return $displayfield;
    }

    public function setDisplayfield(string $value): void
    {
        $this->layer->previewExpression = $value;
    }

    public function getDatasource(): array
    {
        return ( new Datasource( $this->getSimpleXMLElement() ) )->get();
    }

    public function setDatasource(array $datasource, string $sql = '', string $geomcolumn = 'geom'): void
    {
        $datasource['sql'] = $sql;
        $datasource['geomcolumn'] = $geomcolumn;
        ( new Datasource( $this->getSimpleXMLElement() ) )->set($datasource);
    }

    public function getSrid(): int
    {
        return (int) $this->layer->srs->spatialrefsys->srid[0];
    }

    public function getExtent()
    {
        return ( new Extent( $this->getSimpleXMLElement() ) )->get();
    }

    public function setExtent(array $extent): void
    {
        ( new Extent( $this->getSimpleXMLElement() ) )->set($extent);
    }

    public function getOpacity(): float
    {
        return (float) $this->layer->layerOpacity;
    }

    public function setOpacity(float $value): void
    {
        $this->layer->layerOpacity = $value;
    }

    public function getColumns(): array
    {
        return ( new Columns( $this->getSimpleXMLElement() ) )->get();
    }

    public function setColumns(array $columns): void
    {
        ( new Columns( $this->getSimpleXMLElement() ) )->set($columns);
    }

    public function getGeometryType(): string
    {
        return (string) $this->layer['geometry'];
    }

    public function setGeometryType(string $value): void
    {
        $this->layer['geometry'] = $value;
    }

    public function saveAsQml(string $file): string
    {
        $qml = new \Core\Qgis\QmlModel;
        $qml->copyLayerStyle( $this->getSimpleXMLElement() );
        return $qml->save($file);
    }
}