<?php
namespace Core\Qgis\Parsers\Layer;

use \Core\Qgis\Parsers\LayerParser;
use \Core\Adapters\ExtentAdapter;

class Extent extends LayerParser
{
    protected $layer;
    
    public function __construct(\SimpleXMLElement $layer)
    {
        parent::__construct($layer);
    }

	public function get()
	{
		$extent = (array) $this->layer->extent;
		$extent['srid'] = $this->getSrid();
		return ( new ExtentAdapter($extent) )->transform(3857);
    }

    public function set(array $extent): void
	{
        $extent = ( new ExtentAdapter($extent) )->transform( $this->getSrid() );
        $this->layer->extent->xmin = $extent['xmin'];
        $this->layer->extent->ymin = $extent['ymin'];
        $this->layer->extent->xmax = $extent['xmax'];
        $this->layer->extent->ymax = $extent['ymax'];
    }
}