<?php
namespace Core\Qgis\Parsers\Layer;

use \Core\Qgis\Parsers\LayerParser;
use \Core\Qgis\Parsers\Layer\Datasource\{ Db, Ogr, Wms };

class Datasource extends LayerParser
{
	protected $layer; 
	
    public function __construct(\SimpleXMLElement $layer)
    {
        parent::__construct($layer);
    }

    public function get(): array
    {
        switch ( $this->getProvider() ) :
            case 'spatialite': 
			case 'oracle':     
			case 'postgres':   return ( new Db( $this->getSimpleXMLElement() ) )->get();
			case 'wms':        return ( new Wms( $this->getSimpleXMLElement() ) )->get();
            case 'ogr':        return ( new Ogr( $this->getSimpleXMLElement() ) )->get();
        endswitch;
	}
	
    public function set(array $datasource): void
    {
        switch ( $this->getProvider() ) :
            case 'spatialite': 
			case 'oracle':     
			case 'postgres':    
				( new Db( $this->getSimpleXMLElement() ) )->set($datasource);
				break;
			case 'ogr':        
				( new Ogr( $this->getSimpleXMLElement() ) )->set($datasource);
				break;
        endswitch;
    }
}