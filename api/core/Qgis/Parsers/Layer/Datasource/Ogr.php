<?php
namespace Core\Qgis\Parsers\Layer\Datasource;

class Ogr
{
    public function __construct(\SimpleXMLElement $layer)
    {
        $this->layer = $layer;
    }

	public function get(): array
	{
        $datasource = (string) $this->layer->datasource;
        if ( strpos($datasource, '|') !== false ) {
            $split = explode( '|', $datasource);
            $values['file'] = $split[0];
            $values['extension'] = pathinfo( $split[0], PATHINFO_EXTENSION );
            $values['tablename'] = str_replace('layername=', '', $split[1]);
        }
        else {
            $values['file'] = $datasource;
            $values['extension'] = pathinfo($datasource, PATHINFO_EXTENSION);
            $values['tablename'] = pathinfo($datasource, PATHINFO_FILENAME);
        }
        $values['geomcolumn'] = 'geom';
        $values['encoding'] = (string) $this->layer->provider['encoding'];
        return $values;
    }
    
	public function set(array $datasource): void
	{
        $this->layer->datasource = $datasource['file'];
		$this->layer->datasource .= '|layername=';
		$this->layer->datasource .= pathinfo( $datasource['file'], PATHINFO_FILENAME );
		$this->layer->provider = 'ogr';
	}
}