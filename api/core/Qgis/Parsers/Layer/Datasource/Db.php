<?php
namespace Core\Qgis\Parsers\Layer\Datasource;

use \Core\Qgis\Parsers\LayerParser;

class Db extends LayerParser
{
	protected $layer; 

    public function __construct(\SimpleXMLElement $layer)
    {
        parent::__construct($layer);
    }

	public function get(): array
	{
		$datasource = (string) htmlspecialchars_decode($this->layer->datasource);
		$datasource = preg_replace("#\n|\t|\r#", "", $datasource);
		$values = [];
		if ( preg_match('/sql=(.*)/', $datasource, $matches) ) {
		    $datasource = str_replace($matches[0], '', $datasource);
		    $values['sql'] = $matches[1];
        }
		foreach(explode(' ', $datasource) as $token){
		    $kv = explode('=', $token);
		    if(count($kv) == 2){
		        $values[$kv[0]] = str_replace("'", "", $kv[1]);
			}
			else {
		        if(preg_match('/\(([^\)]+)\)/', $kv[0], $matches)){
		            $values['geomcolumn'] = $matches[1];
		        }
		    }
		}
		if( empty($values) ) {
			return $datasource;
		}
		$values['tablename'] = str_replace('"', '', $values['table']);
		unset($values['table']);
		return $values;
	}

	public function set(array $datasource): void
	{
		if (! in_array($datasource['provider'], ['spatialite', 'oracle', 'postgres']) ) {
			throw new \Exception('[LayerParser\Datasource::set] The provider is not a database type : ' . $datasource['provider']);
		}
		// <datasource>dbname='' host= port= user='' password='' key='' srid= type= checkPrimaryKeyUnicity='' table= () sql=</datasource>
		$this->layer->datasource = "dbname='" . $datasource['dbname'] . "' ";
		$this->layer->datasource .= isset($datasource['host']) ? 'host=' . $datasource['host'] . ' ' : '';
		$this->layer->datasource .= isset($datasource['port']) ? 'port=' . $datasource['port'] . ' ' : '';
		$this->layer->datasource .= isset($datasource['user']) ? "user='" . $datasource['user'] . "' " : '';
		$this->layer->datasource .= isset($datasource['password']) ? "password='" . $datasource['password'] . "' " : '';
		$this->layer->datasource .= "key='" . $this->getDisplayfield() . "' ";
		$this->layer->datasource .= "srid='" . $this->getSrid() . "' ";
		$this->layer->datasource .= 'type=' . $this->getGeometryType() . ' ';
		$this->layer->datasource .= "checkPrimaryKeyUnicity='1' ";
		$this->layer->datasource .= 'table=' . $datasource['tablename'] . ' ';
		$this->layer->datasource .= '(' . $datasource['geomcolumn'] . ') ';
		$this->layer->datasource .= 'sql=' . $datasource['sql'];
		$this->layer->provider = $datasource['provider'];
	}
}