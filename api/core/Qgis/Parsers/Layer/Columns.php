<?php
namespace Core\Qgis\Parsers\Layer;

use \Core\Qgis\Parsers\LayerParser;

class Columns extends LayerParser
{
	protected $layer;

    public function __construct(\SimpleXMLElement $layer)
    {
        parent::__construct($layer);
    }

	public function get(): array
	{
		foreach ($this->layer->attributetableconfig->columns->column as $attribute) {
			$attributeName = (string) $attribute['name'];
			$displayfield = $this->getDisplayfield();
			if ( strlen($attributeName) > 0 ) {
				$columns[] = [
					'name' 			=> $attributeName,
					'alias' 		=> $this->getAlias($attributeName),
					'displayfield'	=> ( $attributeName === $displayfield ) ? true : false,
					'excluded' 		=> $this->isExcluded($attributeName),
					'unit'	 		=> '',
					'numeric' 		=> false,
				];
			}

		}
		return $columns;
	}

	public function set(array $columns): void
	{
		$this->updateColumns($columns);
		$this->updateAliases($columns);
		$this->updateExcludedAttributes($columns);
	}
	
	private function getAlias($attributeName): string
	{
		if ( $this->layer->aliases->alias ) {
			foreach ( $this->layer->aliases->alias as $alias ) {
				if ( (string) $alias['field'] === $attributeName ) {
					if ( (string) $alias['name'] !== '' ) {
						return (string) $alias['name'];
					}
				}
			}
		}
		return $attributeName;
	}

	private function isExcluded($attributeName): string
	{
        if ( $this->layer->excludeAttributesWMS->attribute ) {
			foreach ( $this->layer->excludeAttributesWMS->attribute as $excluded ) {
				if ( (string) $excluded === $attributeName ) {
					return true;
				}
            }
        }
        return false;
	}

	private function updateColumns(array $columns): void
	{
		$dom = dom_import_simplexml( $this->layer->attributetableconfig->columns );
		$dom->parentNode->removeChild($dom);
		$this->layer->attributetableconfig->addChild('columns');
		foreach ($columns as $column) {
			$child = $this->layer->attributetableconfig->columns->addChild('column');
			$child->addAttribute('type', 'field');
			$child->addAttribute('hidden', '0');
			$child->addAttribute('width', '-1');
			$child->addAttribute('name', $column['name']);
		}
		$child = $this->layer->attributetableconfig->columns->addChild('column');
		$child->addAttribute('type', 'actions');
		$child->addAttribute('hidden', '0');
		$child->addAttribute('width', '-1');
	}

	private function updateAliases(array $columns): void
	{
		$dom = dom_import_simplexml( $this->layer->aliases );
		$dom->parentNode->removeChild($dom);
		$this->layer->addChild('aliases');
		$index = 0;
		foreach ($columns as $column) {
			$child = $this->layer->aliases->addChild('alias');
			$child->addAttribute('field', $column['name']);
			$child->addAttribute('index', $index);
			$child->addAttribute('name', ($column['name'] === $column['alias']) ? '' : $column['alias']);
			$index++;
		}
	}

	private function updateExcludedAttributes(array $columns): void
	{
		$dom = dom_import_simplexml( $this->layer->excludeAttributesWMS );
		$dom->parentNode->removeChild($dom);
		$dom = dom_import_simplexml( $this->layer->excludeAttributesWFS );
		$dom->parentNode->removeChild($dom);
		$this->layer->addChild('excludeAttributesWMS');
		$this->layer->addChild('excludeAttributesWFS');
		foreach ($columns as $column) {
			if ($column['excluded']) {
				$this->layer->excludeAttributesWMS->addChild('attribute', $column['name']);
				$this->layer->excludeAttributesWFS->addChild('attribute', $column['name']);
			}
		}
	}
}
