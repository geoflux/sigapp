<?php
namespace Core\Qgis;

use \Core\Qgis\Adapters\{ LayersAdapter, DumpAdapter };

class QgisAdapter extends AbstractQgis
{
    protected $projectfile;
    private $qgis;
    
    public function __construct(string $projectfile)
    {
        $this->projectfile = $projectfile;
        $this->getSimpleXMLElement();
    }

    public function getQgisProject(): array
    {
        return [
            'file'      => $this->projectfile,
            'name'      => $this->getName(),
            'title'     => $this->getTitle(),
            'version'   => $this->getVersion()
        ];        
    }

    public function getLayersAdapter(): LayersAdapter
    {
        return new LayersAdapter( $this );        
    }

    public function getLayersDump(): array
    {
        $layers = new DumpAdapter( $this );
        return $layers->getDumpSettings();
    }
}