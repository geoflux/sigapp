<?php
namespace Core\Qgis;

use \Core\Qgis\Parsers\{ CanvasParser, LayerParser };

abstract class AbstractQgis
{
    protected $projectfile;
    private $qgis;

    public function getProjectfile()
    {
        return $this->projectfile;
    }

    protected function getSimpleXMLElement(): void
    {
        $this->qgis = simplexml_load_file( $this->projectfile );
    }
        
    /**
     * Save modifications in the projectfile
     */
    public function save(): void
    {
        $this->qgis->asXml( $this->projectfile );
    }
        
    /**
     * Return projectfile version (ex: 3.4.15-Madeira)
     */
    public function getVersion(): string
    {
        return (string) $this->qgis['version'];
    }
    
    /**
     * Return project filename (ex: c:/projects/map.qgs => map)
     */
    public function getName(): string
    {
        return pathinfo( $this->projectfile, PATHINFO_FILENAME );
    }
    
    /**
     * Return project title from qgis->title node
     */
    public function getTitle(): string
    {
        return (string) $this->qgis->title;
    }
    
    /**
     * Add or replace the title of the project
     */
    public function setTitle(string $value): void
    {
        $this->qgis->title = $value;
        $this->save();
    }
    
    /**
     * Tells QGIS Server to use <layerid> instead of name parameters which is not necessarily specified
     */
    public function useLayerIdAsName(): void
    {
        if ( empty($this->qgis->properties->WMSUseLayerIDs) ) {
            $this->qgis->properties->addChild('WMSUseLayerIDs');
            $this->qgis->properties->WMSUseLayerIDs->addAttribute('type', 'bool');
        }
        if ( empty($this->qgis->properties->WFSUseLayerIDs) ) {
            $this->qgis->properties->addChild('WFSUseLayerIDs');
            $this->qgis->properties->WFSUseLayerIDs->addAttribute('type', 'bool');
        }
        $this->qgis->properties->WMSUseLayerIDs = 'true';
        $this->qgis->properties->WFSUseLayerIDs = 'true';
        $this->save();
    }
    
    /**
     * Access to <mapcanvas> node
     */
    public function getCanvas(): CanvasParser
    {
        foreach ( $this->qgis->mapcanvas as $mapcanvas ) {
           if ( (string) $mapcanvas->attributes()['name'] === 'theMapCanvas') {
                return new CanvasParser($mapcanvas);
           }
        }
    }
    
    /**
     * Return LayerParser instances from <maplayer> nodes in array
     */
    public function getLayers(): array
    {
        foreach ( $this->qgis->projectlayers->maplayer as $layer ) {
            $layers[] = new LayerParser($layer);
        }
        $layers = isset($layers) ? $layers : [];
        return $layers;
    }
    
    /**
     * Find specific layer in <maplayer> nodes and return LayerParser instance
     */
    public function getLayerByName(string $name): ?LayerParser
    {
        $layers = $this->getLayers();
        foreach ( $layers as $layer ) {
            $names[] = $layer->getName();
        }
        $key = array_search($name, $names);
        if ($key === false) {
            return null;
        }
        return $layers[$key];
    }
    
    /**
     * Remove a <maplayer> node by name
     */
    public function removeLayer(string $name): void
    {
        $layer = $this->getLayerByName($name)->getSimpleXMLElement();
        $dom = dom_import_simplexml($layer);
        $dom->parentNode->removeChild($dom);
        $this->save();
    }
    
    /**
     * Add a <maplayer> node in the project
     */
    public function addLayer(\SimpleXmlElement $layer): void
    {
        $layer = dom_import_simplexml($layer);
        $projectlayers = dom_import_simplexml($this->qgis->projectlayers);
        $projectlayers->appendChild( $projectlayers->ownerDocument->importNode($layer, true) );
        $this->save();
    }

     /**
     * Set <layer-tree-group> according to maplayers
     */   
    public function updateLayerTree(): void
    {
		$dom = dom_import_simplexml( $this->qgis->{'layer-tree-group'} );
		$dom->parentNode->removeChild($dom);
        $treeGroup = $this->qgis->addChild('layer-tree-group');
        foreach ( $this->getlayers() as $layer ) {
            $treeLayer = $treeGroup->addChild('layer-tree-layer');
            $treeLayer['id'] = $layer->getName();
            $treeLayer['name'] = $layer->getTitle();
            $treeLayer->addChild('customproperties');
        }
        $this->save();
    }
}