<?php
namespace Core\Datastore;

use \Core\Datastore\Manager\{ Dump, Extract, Drop };

class DatastoreManager
{
    public static function dump(array $data)
    {
        $dump = new Dump($data);
        $dump->fileExists()->process()->updateDatasource();
        return self::log($data);
    }

    public static function drop(string $tablename)
    {
        $drop = new Drop($tablename);
        $drop->deleteTableIfExists();
        return self::unlog($tablename);
    }

    private static function log(array $data)
    {
        return DatastoreModel::create($data);
    }

    private static function unlog(string $tablename)
    {
        return DatastoreModel::where('tablename', $tablename)->delete();
    }
}