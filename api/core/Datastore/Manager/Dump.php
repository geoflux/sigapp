<?php
namespace Core\Datastore\Manager;

use Symfony\Component\Process\Process;
use \Core\Datastore\AbstractDatastore;

class Dump extends AbstractDatastore
{
    protected $project_name;
    protected $layer_name;
    protected $tablename;
    protected $tablename_origin;
    protected $geomtype;
    protected $key;
    protected $file;
    protected $projectfile;

    public function __construct(array $data)
    {
		foreach ( $data as $k => $v ) {
            $this->{$k} = $v;
        }
        $this->getParams();
    }

    public function fileExists(): Dump
    {
        if ( !file_exists($this->file) ) {
            $error = 'Missing local data source : ' . pathinfo( $this->file, PATHINFO_BASENAME);
            throw new DumpException($error, $this->projectfile, $this->layer_name);
        }
        return $this;
    }

    public function process(): Dump
    {
        $prefix = ($this->params['driver'] === 'PostgreSQL') ? 'PG:' : '';
        $process = new Process([
            'ogr2ogr',
            '-f',  $this->params['driver'],
            $prefix . $this->params['database'], $this->file,
            '-sql', "SELECT * FROM " . $this->tablename_origin,
            '-dialect', 'spatialite',
            '-nlt',  $this->geomtype,
            '-lco', 'SCHEMA=datastore',
            '-lco', 'GEOMETRY_NAME=geom',
            '-lco', 'FID=' . $this->key,
            '-nln', $this->tablename,
            '-dim',  'XY',
            '-append'
        ]);
        $process->run();
		if (!$process->isSuccessful()) {
            $error = '"' . $this->tablename_origin . '" data source appears to be non-compliant. The layer could not be loaded with ogr2ogr. ' . $process->getErrorOutput();
            throw new DumpException($error, $this->projectfile, $this->layer_name);
        }
        return $this;
    }

    public function updateDatasource(): void
    {
        $datasource = [
            'projectfile' => $this->projectfile,
            'layer_name'  => $this->layer_name,
            'dbname'      => $this->params['dbname'],
            'tablename'   => ($this->params['driver'] === 'PostgreSQL') ? '"datastore"."' . $this->tablename . '"' : $this->tablename,
            'provider'    => $this->params['provider']
        ];
        if ($this->params['driver'] === 'PostgreSQL') {
            array_merge($datasource, [
                'host'      => $this->params['host'],
                'port'      => $this->params['port'],
                'user'      => $this->params['user'],
                'password'  => $this->params['password'],
            ]);
        }
        $this->setDatasource($datasource);
    }
}