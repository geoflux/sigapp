<?php
namespace Core\Datastore\Manager;

use \Core\Datastore\AbstractDatastore;

class Drop extends AbstractDatastore
{
    public $tablename;
    protected $db;

    public function __construct(string $tablename)
    {
        $this->tablename = $tablename;
        $this->connect();
    }

    public function deleteTableIfExists()
    {
        return $this->db->exec('
            DROP TABLE IF EXISTS ' .  $this->tablename . '
        ');
    }
}