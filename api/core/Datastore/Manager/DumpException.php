<?php
namespace Core\Datastore\Manager;

use \Core\Qgis\QgisModel;

class DumpException extends \Exception
{
    public function __construct(string $message, string $projectfile, string $layername)
    {
        $this->message = '[DATASTORE ERROR] ' . $message;
        $qgis = new QgisModel($projectfile);
        $qgis->removeLayer($layername);
    }
}