<?php
namespace Core\Datastore;

use \Database\Datastore;
use \PDO;
use \Core\Qgis\QgisModel;

abstract class AbstractDatastore
{
    protected $db;
    protected $params;

    protected function connect()
    {
        $this->db = Datastore::connect();
    }

    protected function getParams()
    {
        $this->params = Datastore::getParams();
    }

    protected function setDatasource(array $datasource)
    {
        $qgis = new QgisModel( $datasource['projectfile'] );
        $qgis->getLayerByName( $datasource['layer_name'] )->setDatasource($datasource);
        $qgis->save();       
    }
}