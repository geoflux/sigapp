<?php
namespace Core\File;

trait FileTrait
{
    public $name;

    protected function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTempdir(): string
    {
        return pathinfo( $this->name, PATHINFO_DIRNAME ) . '/';
    }

    protected function getFormat(): string
    {
        return pathinfo( $this->name, PATHINFO_EXTENSION );
    }

    protected function getFilename(): string
    {
        return pathinfo($this->name, PATHINFO_FILENAME);
    }    
}