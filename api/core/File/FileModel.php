<?php
namespace Core\File;

use Symfony\Component\Filesystem\Filesystem;

class FileModel
{
    use FileTrait;
    
    public $name;
    public $errors;

    public function __construct(string $file = null)
    {
        $this->name = $file;
        $this->errors = [];
    }

    public function moveToTempdir(\Slim\Http\UploadedFile $uploadedFile): FileModel
    {
        $clientFilename = $uploadedFile->getClientFilename();
        $tempdir = PATH_TO_MAPS . uniqid('TEMP_') . '/';
        ( new Filesystem() )->mkdir( $tempdir );
        $this->setName($tempdir . $clientFilename);
        $uploadedFile->moveTo( $this->name );
        return $this;
    }

    public function getProjectfile(): FileModel
    {
        switch ( $this->getFormat() ) {
            case 'zip': 
                $this->extract()->normalize()->storeLocalDatasets();
                break;
            case 'qgz': 
                $this->extract()->normalize();
                break;
            case 'qgs': 
                $this->normalize();
                break;        
            default:
                throw new FileException('Invalid file format : '. $this->ext, $this->name);
        }
        return $this->rename();
    }

    private function normalize(): FileModel
    {
        $file = new FileNormalizer($this->name);
        $file->normalize();
        $this->setName( $file->name );
        return $this;
    }

    private function extract(): FileModel
    {
        $archive = new FileArchive($this->name);
        $archive->extract()->getProjectfile();
        $this->setName( $archive->name );
        return $this;
    }

    private function storeLocalDatasets(): FileModel
    {
        $storage = new FileDatastore($this->name);
        $storage->dump();
        $this->errors = $storage->errors;
        return $this;
    }

    private function rename(): FileModel
    {
        $file = PATH_TO_MAPS . $this->getFilename() . '.' . $this->getFormat();
        ( new Filesystem )->rename( $this->name, $file );
        ( new Filesystem )->remove( $this->getTempdir() );
        $this->setName($file);
        return $this;
    }
}