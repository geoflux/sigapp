<?php
namespace Core\File;

use \Core\Qgis\QgisModel;
use Symfony\Component\Filesystem\Filesystem;
use Cocur\Slugify\Slugify;

class FileNormalizer extends QgisModel
{
    use FileTrait;
    
    public $name;
    private $qgis;

    public function __construct(string $file)
    {
        $this->name = $file;
        parent::__construct($file);
    }

    public function normalize()
    {
        $this->defineProjectTitle();
        $this->useLayerIdAsName();
        $this->updateLayerTree();
        $this->rename();
        return $this;
    }

    private function defineProjectTitle()
    {
        $title = $this->getTitle();
        if ( empty($title) || strlen($title === 0) ) {
            $this->setTitle( $this->getFilename() );
        }   
    }

    private function rename()
    {
        $string = new Slugify(['separator' => '_']);
        $filename = uniqid( $this->getFilename() . '_');
        $file = $this->getTempdir() . $string->slugify( $filename ) . '.qgs';
        ( new Filesystem )->rename( $this->name, $file );        
        $this->setName($file);
    }
}