<?php
namespace Core\File;

use PhpZip\ZipFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class FileArchive
{
    use FileTrait;

    public $name;
    private $tempdir;
    private $content;

    public function __construct(string $file)
    {
        $this->name = $file;
        $this->tempdir = $this->getTempdir();
        $this->content = $this->tempdir . uniqid() . '/';
    }

    public function extract(): FileArchive
    {
        ( new Filesystem )->mkdir( $this->content );
        $zipFile = new ZipFile();
        try {
            $zipFile
                ->openFile( $this->name ) 
                ->extractTo( $this->content )
                ->close();
        } catch(\Exception $e) {
            $zipFile->close();
            $error = 'Archive seems corrupted. ' . $e->getMessage() . ' - ' . $this->name;
            throw new FileException($error, $this->name);
        } 
        ( new Filesystem )->remove( $this->name );
        return $this;
    }

    public function getProjectfile(): FileArchive
    {
        $this->find();
        if ( $this->getFormat() === 'qgz' ) {
            return ( new self($this->name) )->extract()->getProjectfile();
        }
        return $this;
    }

    protected function find(): void
    {
        $finder = new Finder();
        $finder->in( $this->content );
        $finder->files()->name(['*.qgz', '*.qgs']);
        foreach ($finder as $file) {
            $project[] = $file->getRealPath();
        }
        if ( !isset($project) ) {
            throw new FileException('No project found in the archive', $this->name);
        }
        $this->setName( $this->tempdir . pathinfo($project[0], PATHINFO_BASENAME) );
        ( new Filesystem )->rename( $project[0], $this->name );
    }

    public function getContent(): string
    {
        return $this->content;
    }
}