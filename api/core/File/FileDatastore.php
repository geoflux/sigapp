<?php
namespace Core\File;

use \Core\Qgis\QgisAdapter;
use \Core\Datastore\DatastoreManager;

class FileDatastore extends QgisAdapter
{
    public $errors;

    public function __construct(string $file)
    {
        parent::__construct($file);
        $this->errors = [];
    }

    public function dump()
    {
        $layers = $this->getLayersDump();
        foreach ($layers as $layer) {
            try {
                DatastoreManager::dump($layer);
            } catch (\Exception $e) {
                array_push( $this->errors, $e->getMessage() );
            }
        }
        return $this;
    }
}