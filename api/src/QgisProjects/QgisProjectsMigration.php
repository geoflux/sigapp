<?php
namespace Sigapp\QgisProjects;

use \Core\Qgis\QgisAdapter;
use \Sigapp\Basemaps\BasemapsManager;
use \Sigapp\Layers\LayersManager;

final class QgisProjectsMigration extends QgisAdapter
{
    public $project;
    protected $projectfile;
    private $options;
    public $errors;
    public $count;

    public function __construct(\Core\File\FileModel $file, array $options = [])
    {
        parent::__construct($file->name);
        $this->options = $this->setOptions($options);
        $this->errors = $file->errors;
        $this->count = 0;
    }

    public function store()
    {
        $this
            ->storeQgisProject()
            ->storeLayers()
            ->storeBasemaps();
    }

    private function storeQgisProject()
    {
        $project =  QgisProjectsModel::create( $this->getQgisProject() );
        $this->project = $project->name;
        return $this;
    }

    private function storeLayers()
    {
        $layers = $this->getLayersAdapter()->filterByProviders(['spatialite', 'postgres', 'oracle']);
        foreach ( $layers as $layer ) {
            $layer['folder_id'] = $this->options['folder_id'];
            $layer['role_id'] = $this->options['role_id'];
            $layer = ( new LayersManager($layer) )->create();
        }
        $this->count = $this->count + count($layers);
        return $this;
    }

    private function storeBasemaps()
    {
        $basemaps = $this->getLayersAdapter()->filterByProviders(['wms']);
        foreach ( $basemaps as $basemap ) {
            $basemap = ( new BasemapsManager($basemap) )->create();
        }
        $this->count = $this->count + count($basemaps);
        return $this;     
    }

    private function setOptions($options)
    {
        $default = [ 'folder_id' => 0,  'role_id' => 4 ];
        return array_merge($default, $options);
    } 

}