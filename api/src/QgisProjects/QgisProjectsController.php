<?php
namespace Sigapp\QgisProjects;

use \Core\File\FileModel;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class QgisProjectsController
{
    public function index(Request $request, Response $response)
    {
        $projects = QgisProjectsModel::orderBy('title')->with('layers')->get();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($projects);
    }

    public function store(Request $request, Response $response)
    {
        $options = (array) json_decode( $request->getParsedBody()['options'] );
        $uploadedFile = $request->getUploadedFiles()['file'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $file = new FileModel;
            $file->moveToTempdir($uploadedFile)
                ->getProjectfile();
            $project = new QgisProjectsMigration($file, $options);
            $project->store();
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withJson( $project );
        } else {
            return $response
                ->withStatus(500)
                ->withHeader('Content-Type', 'application/json')
                ->withJson( $uploadedFile->getError() );
        }
    }

    public function destroy(Request $request, Response $response, $args)
    {
        $project = QgisProjectsModel::where('name', $args['name'] );
        \Sigapp\Layers\LayersManager::deleteByProjectName( $args['name'] );
        unlink(PATH_TO_MAPS . $args['name'] . '.qgs');
        QgisProjectsModel::where('name', $args['name'] )->delete();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($project);
    }  

    public function previews(Request $request, Response $response, $args)
    {
        $project = QgisProjectsModel::where('name', $args['name'] )->first();
        foreach ($project->layers as $layer) {
            $thumbnails[] = ( new \Sigapp\Layers\LayersThumbnails($layer['id']) )->create();
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($thumbnails);
    }
}