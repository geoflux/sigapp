<?php
namespace Sigapp\Basemaps;

use \Core\Qgis\QgisModel;

class BasemapsManager
{
    public function __construct(array $basemap)
    {
        $this->basemap = $basemap;
    }

    public function create()
    {
        $exists = BasemapsModel::where([
            [ 'url', '=', $this->basemap['url'] ],
            [ 'layer', '=', $this->basemap['layer'] ],
        ])->first();
        if ( $exists !== null ) {
            return null;
        }
        $basemap = BasemapsModel::create($this->basemap);
        $this->store();
        return $basemap;
    }

    private function store()
    {
        $qgis = new QgisModel( PATH_TO_MAPS . $this->basemap['project_name'] . '.qgs' );
        $maplayer = $qgis->getLayerByName( $this->basemap['name'] )->getSimpleXMLElement();
        $qgis->removeLayer( $this->basemap['name'] );
        $qgis = new QgisModel( PATH_TO_MAPS . 'template.basemaps.qgs' );
        $qgis->addLayer($maplayer);
    }
}