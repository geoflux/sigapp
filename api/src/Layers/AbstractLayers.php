<?php
namespace Sigapp\Layers;

use \Sigapp\Layers\Providers\{ Postgis, Spatialite, Oracle };
use \PDO;
use Spatialite\SPL;
use \Core\Qgis\QgisModel;

abstract class AbstractLayers
{
    public $id;
    public $name;
    public $title;
    public $tablename;
    public $displayfield;
    public $geomcolumn;
    public $geomtype;
    public $srid;
    public $extent;
	public $sql;
	public $columns;
    public $datatable;
    public $datasource;
    public $db;

    public function getLayer()
    {
        $data = LayersDataModel::find($this->id);
        $this->name         = $data->name;
        $this->title        = $data->layer->title;
        $this->project_name = $data->project_name;
        $this->tablename    = $data->tablename;
        $this->displayfield = $data->displayfield;
        $this->geomcolumn   = $data->geomcolumn;
        $this->geomtype     = $data->geomtype;
        $this->srid         = $data->srid;
        $this->extent       = $data->extent;
		$this->sql          = $data->sql;
		$this->columns		= $data->columns;
        $this->datatable    = $data->datatable;
        $this->datasource   = $data->datasource;        
    }

	public function getProvider()
	{
        $db = $this->datasource;
		if ($db->provider === 'spatialite') {
			$this->db = new SPL( $db->dbname );
			return new Spatialite($this);
		} 
		elseif ($db->provider === 'postgres') {
			$this->db = new PDO('pgsql:host=' . $db->host . ';port=' . $db->port . ';dbname=' . $db->dbname . ';user=' . $db->user . ';password=' . $db->password);
			return new Postgis($this);
		} 
		elseif ($db->provider === 'oracle') {
			$this->db = new PDO('oci:dbname=' . $db->host . ':' . $db->port .  '/' . $db->dbname, $db->user, $db->password);
			return new Oracle($this);
		} 
		else {
			return false;
		}
    }

    public function getQgisLayer()
    {
        $project = new QgisModel(PATH_TO_MAPS . $this->project_name . '.qgs');
        return $project->getLayerByName($this->name);
    }
}