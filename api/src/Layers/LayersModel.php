<?php
namespace Sigapp\Layers;

use Illuminate\Database\Eloquent\Model;

class LayersModel extends Model
{
	protected $table = 'layers';

	protected $casts = [
        'folder_id' => 'integer',
        'role_id' => 'integer',
    ];
    
	protected $fillable = [
		"name", 
		"title", 
		"abstract", 
		"source", 
		"date", 
		"project_name", 
		"map", 
		"symbol_url", 
		"symbol_type", 
		"preview_url", 
		"protocol", 
		"folder_id", 
		"role_id"
	];

    public function data()
    {
        return $this->hasOne('\Sigapp\Layers\LayersDataModel', 'layer_id', 'id');
	}   
}