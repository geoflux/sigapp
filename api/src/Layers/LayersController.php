<?php
namespace Sigapp\Layers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class LayersController
{
    public function index(Request $request, Response $response, $args)
    {
        $layer = LayersModel::find( $args['id'] );
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($layer);
    }

    public function update(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $layer = LayersModel::find( $args['id'] );
        $layer->update($data);
        $qgislayer = new LayersQgisUpdater($layer);
        $qgislayer->update();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($layer);
    }

    public function folder(Request $request, Response $response, $args)
    {
        $layer = LayersModel::find( $args['id'] );
        $layer->folder_id = $args['folder_id'];
        $layer->save();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($layer);
    }

    public function project(Request $request, Response $response, $args)
    {
        $layer = LayersModel::find( $args['id'] );
        $qgislayer = new LayersQgisUpdater($layer);
        $result = $qgislayer->moveTo( $args['project_name'] );
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($result);
    }
    
    public function previews(Request $request, Response $response, $args)
    {
        $thumbnails = ( new LayersThumbnails($args['id']) )->create();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($thumbnails);
    }

    public function destroy(Request $request, Response $response, $args)
    {
        $layer = LayersModel::find( $args['id'] );
        LayersManager::deleteById( $args['id'] );
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($layer);
    }    
}