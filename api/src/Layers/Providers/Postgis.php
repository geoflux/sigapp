<?php
namespace Sigapp\Layers\Providers;

use \PDO;
use \Sigapp\Layers\{ LayersDataEntity, LayersDataEntityInterface };

class Postgis implements LayersDataEntityInterface
{
	protected $tablename;
    protected $displayfield;
    protected $geomcolumn;
    protected $srid;
    protected $extent;
    protected $sql;
    protected $datatable;
	protected $db;

	public function __construct(\Sigapp\layers\AbstractLayers $layer)
	{
        $this->tablename    = $layer->tablename;
        $this->displayfield = $layer->displayfield;
        $this->geomcolumn   = $layer->geomcolumn;
        $this->srid         = $layer->srid;
        $this->extent       = $layer->extent;
        $this->sql          = $layer->sql;
        $this->datatable    = $layer->datatable;
        $this->db   		= $layer->db;
	}

	public function getDatatable(string $filter)
	{
		$datatable = $this->db->query("
			SELECT " . $this->datatable . "
			FROM " . $this->tablename . $this->getFilter($filter) . "
			LIMIT 2000
		")->fetchAll(PDO::FETCH_ASSOC);
		if ( !isset($datatable) ) {
			return false;
		}
		return $datatable;
	}

	public function getDatatableFromExtent(array $extent, string $filter)
	{
		$datatable = $this->db->query("
			SELECT " . $this->datatable ."
			FROM " . $this->tablename . " 
			" . $this->whereIntersects($extent) . "
			" . $this->getFilter($filter, true) . "
			LIMIT 2000
		")->fetchAll(PDO::FETCH_ASSOC);
		if ( !isset($datatable) ) {
			return false;
		}
		return $datatable;
	}

	public function getExtent($filter)
	{
		return $this->db->query("
			WITH extent AS(
				SELECT ST_Extent(ST_Transform(ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . "), 3857)) AS geom
				FROM " . $this->tablename . $this->getFilter($filter) . "
			)
			SELECT
				ST_XMin(geom) AS xmin,
				ST_YMin(geom) AS ymin,
				ST_XMax(geom) AS xmax,
				ST_YMax(geom) AS ymax,
				3857 AS srid
			FROM extent
		")->fetch(PDO::FETCH_ASSOC);
	}
    
	public function getFeatureInfo($identifier)
	{
		$result = $this->db->query("
			SELECT " . $this->datatable . ",
			ST_AsGeoJson(ST_Transform(ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . "), 4171), 5, 0) AS geojson
			FROM " . $this->tablename . "
			WHERE \"" . $this->displayfield . "\" = '" . trim($identifier) . "';
		")->fetch(PDO::FETCH_OBJ);
		$result->geojson = mb_convert_encoding($result->geojson,  "ISO-8859-1");
		return $result;
	}

	public function getFeatureInfoFromCoordinates($x, $y, $zoom, $filter)
	{
		$result = $this->db->query("
			WITH pointer AS (
				SELECT ST_Transform(ST_SetSRID(ST_MakePoint(". $x .", ". $y ."), 3857), " . $this->srid . ") AS p_geom
			)
			SELECT DISTINCT \"" . $this->displayfield . "\" AS displayfield,
			MIN(ST_Distance(p_geom, ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . ")))
			FROM " . $this->tablename . ", pointer
			WHERE ST_Distance(p_geom, ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . ")) <= (3000000 / POW($zoom, 4))
			" . $this->getFilter($filter, true) . "
			GROUP BY \"" . $this->displayfield . "\", \"" . $this->geomcolumn . "\"
			ORDER BY min
			LIMIT 1
		")->fetch(PDO::FETCH_ASSOC);
		if ($result) {
			return $this->getFeatureInfo( $result['displayfield'] );
		}
		return false;		
	}

	public function getEntityExtent($identifier)
	{
		return $this->db->query("
			SELECT
			ST_XMin(ST_Transform(ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . "), 3857)) AS xmin,
			ST_YMin(ST_Transform(ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . "), 3857)) AS ymin,
			ST_XMax(ST_Transform(ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . "), 3857)) AS xmax,
			ST_YMax(ST_Transform(ST_SetSRID(" . $this->geomcolumn . ", " . $this->srid . "), 3857)) AS ymax,
			3857 AS srid
			FROM " . $this->tablename . "
			WHERE \"" . $this->displayfield . "\" = '" . trim($identifier) . "'
		")->fetch(PDO::FETCH_ASSOC);
	}
	
	public function getDistinctValues($attribute)
	{
        $result = $this->db->query("
            SELECT DISTINCT $attribute AS value
            FROM " . $this->tablename . "
            ORDER BY $attribute
            LIMIT 150
        ")->fetchAll(PDO::FETCH_OBJ);
		foreach($result as $row){
			$values[] =  (is_numeric($row->value)) ? $row->value : "'" . $row->value . "'" ;
		}
        return $values;
	}	
	
	public function whereIntersects($extent)
	{
		$polygon = "ST_MakePolygon( 
			ST_GeomFromText( 
				'LINESTRING(
					" . $extent['xmin'] . " " . $extent['ymin']  . ", 
					" . $extent['xmin'] . " " . $extent['ymax']  . ", 
					" . $extent['xmax'] . " " . $extent['ymax']  . ", 
					" . $extent['xmax'] . " " . $extent['ymin']  . ", 
					" . $extent['xmin'] . " " . $extent['ymin']  . "
				)' 
			) 
		)";
		$polygonTransformed = "ST_Transform( ST_SetSRID( $polygon, " . $extent['srid'] . " ), " . $this->srid . ")";
		return " WHERE ST_Intersects($polygonTransformed, " . $this->geomcolumn . ")"; 
	}

	public function getFilter($filter, $precondiction = false)
	{
		$filter = new Filter($this->sql, $filter, $precondiction);
		return $filter->get();
	}
	
}