<?php
namespace Sigapp\Layers;

use \Sigapp\Datasources\DatasourcesManager;
use \Sigapp\Maps\MapsLayersModel;
use \Core\Datastore\DatastoreManager;
use Symfony\Component\Filesystem\Filesystem;

class LayersManager
{
    public function __construct(array $layer)
    {
        $this->layer = $layer;
        unset($this->layer['properties'], $this->layer['datasource']);
        $this->layerData = $layer['properties'];
        $this->layerData['datasource_id'] = ( new DatasourcesManager($layer['datasource']) )->createIfNotRecorded();
    }

    public function create()
    {
        $layer = LayersModel::create($this->layer);
        $this->layerData['layer_id'] = $layer->id;
        LayersDataModel::create($this->layerData);
        self::updateDatatableQuery( $this->layerData['layer_id'] );
        return $layer;
    }

    public static function createThumbnails($project_name)
    {
        foreach( LayersModel::where('project_name', $project_name)->get() as $layer ) {
            ( new LayersThumbnails($layer->id) )->create();     
        }
    }

	public static function updateDatatableQuery($layer_id) 
	{
		$layer = LayersDataModel::find($layer_id);
		foreach($layer->columns as $column){
			if ( !$column['excluded'] && strlen($column['name']) > 0 ) {
				$str[] = '"' . $column['name'] . '" AS "' . $column['alias'] . '"';
			}
		}
		$query = implode(', ', $str);
		$layer->datatable =  '"' . $layer->displayfield . '" AS "ID", ' . $query;
		$layer->save();
    }

    public static function deleteByProjectName($project_name)
    {
        $layers = LayersModel::where('project_name', $project_name)->get();
        foreach ($layers as $layer) {
            self::deleteById($layer->id);
        }
    }

    public static function deleteById($layer_id)
    {
        $tablename = LayersModel::find($layer_id)->data->tablename;
        LayersModel::find($layer_id)->delete();
        LayersDataModel::where('layer_id', $layer_id)->delete();
        MapsLayersModel::where('layer_id', $layer_id)->delete();
        DatastoreManager::drop( $tablename );
        ( new Filesystem )->remove( PATH_TO_IMAGES . 'layer_preview_' .  $layer_id . '.png' );       
        ( new Filesystem )->remove( PATH_TO_IMAGES . 'layer_symbol_' .  $layer_id . '.png' );       
    }
}