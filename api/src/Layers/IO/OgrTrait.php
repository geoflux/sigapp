<?php
namespace Sigapp\Layers\IO;

use Symfony\Component\Filesystem\Filesystem;

trait OgrTrait
{
    public $id;

    public function getSourceData()
    {
        $db = $this->datasource;
        if ($db->provider === 'postgres') {
            return "PG:dbname='". $db->dbname ."' host='". $db->host ."' port='". $db->port ."' user='". $db->user ."' password='". $db->password ."'";
        }
        elseif ($db->provider === 'spatialite') {
            return $db->dbname;
        }
    }

    public function getDestinationData()
    {
        if ($this->format === 'shp') {
            return $this->tempdir . $this->title . '.shp';
        }
        elseif ($this->format === 'geojson') {
            return $this->tempdir . $this->title . '.geojson';           
        }
    }

    public function getDriverData()
    {
        if ($this->format === 'shp') {
            return 'ESRI Shapefile';
        }
        elseif ($this->format === 'geojson') {
            return 'GeoJSON';
        }        
    }

    public function createTempdir()
    {
        $this->tempdir = PATH_TO_FILES . uniqid() . '/';
        ( new Filesystem() )->mkdir( $this->tempdir );
        return $this;
    }

    public function getSqlQuery()
    {
		foreach($this->columns as $column){
			if ( !$column['excluded'] && strlen($column['name']) > 0 ) {
				$str[] = '"' . $column['name'] . '"';
            }
		}        
        $query = "SELECT " . implode(', ', $str) . ', ' . $this->geomcolumn . " FROM " . $this->tablename;
        if ( !empty($this->inExtent) ) {
            $query .= $this->getProvider()->whereIntersects($this->inExtent);
        }
        $query .= $this->getProvider()->getFilter($this->filter, true);
        $query = preg_replace('#\n|\t|\r#', ' ', trim(utf8_encode($query)));
        return $query;
    }

    public function createQml()
    {
        $this->getQgisLayer()->saveAsQml($this->tempdir . $this->title . '.qml');
    }

    public function getOutput()
    {
        $zip = new Zip($this->tempdir);
        $output = $zip->addFiles();
        ( new Filesystem )->remove($this->tempdir);
        return $output;
    }
}