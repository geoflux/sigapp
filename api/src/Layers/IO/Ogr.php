<?php
namespace Sigapp\Layers\IO;

use Symfony\Component\Process\Process;
use \Sigapp\Layers\AbstractLayers;

class Ogr extends AbstractLayers
{
    use OgrTrait;
    public $id;

    public function __construct($id, $format, $filter, $extent)
    {
        $this->id = $id;
        $this->format = $format;
        $this->filter = $filter;
        $this->inExtent = $extent;
        $this->getLayer();
    }

    public function process(): Ogr
    {
        $this->createTempdir();
        $process = new Process([
            'ogr2ogr',
            '-f',  $this->getDriverData(),
            $this->getDestinationData(), $this->getSourceData(),
            '-sql', $this->getSqlQuery()
        ]);
        $process->run();
		if (!$process->isSuccessful()) {
            $error = "Exctraction issue. " . $process->getErrorOutput();
            // $error = "Exctraction issue. " . $process->getCommandLine();
            throw new OgrException($error, $this);
        }
        return $this;
    }

    public function create()
    {
        $this->process();
        $this->createQml();
        return $this->getOutput();
    }
}