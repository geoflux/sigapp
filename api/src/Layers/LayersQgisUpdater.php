<?php
namespace Sigapp\Layers;

use \Core\Qgis\QgisModel;
use \Core\Qgis\Parsers\LayerParser;
use \Sigapp\Datasources\DatasourcesModel;

class LayersQgisUpdater
{
    public function __construct(LayersModel $layer)
    {
        $this->layer = $layer;
        $this->datasource = DatasourcesModel::find($this->layer->data->datasource_id);
        $this->qgis = new QgisModel($this->layer->map);
        $this->qgislayer = $this->qgis->getLayerByName($this->layer->name);
    }

    public function update()
    {
        $this->qgislayer->setTitle( $this->layer->title );
        $this->qgislayer->setAbstract( $this->layer->abstract );
        $this->qgislayer->setSource( $this->layer->source );
        $this->qgis->save();
    }

    public function updateGeometryType()
    {
        $this->qgislayer->setGeometryType($this->layer->data->geomtype);
        $this->qgis->save();
    }

    public function updateDisplayfield()
    {
        $this->qgislayer->setDisplayfield($this->layer->data->displayfield);
        $this->qgis->save();
    }

    public function updateColumns()
    {
        $this->qgislayer->setColumns($this->layer->data->columns);
        $this->qgis->save();
    }

    public function updateSrid()
    {
        $this->qgislayer->setSrid($this->layer->data->srid);
        $this->qgis->save();
    }

    public function updateExtent()
    {
        $this->qgislayer->setExtent($this->layer->data->extent);
        $this->qgis->save();
    }

    public function updateDatasource()
    {
        $datasource = [
            'dbname'     => $this->datasource->dbname,
            'host'       => $this->datasource->host,
            'port'       => $this->datasource->port,
            'user'       => $this->datasource->user,
            'password'   => $this->datasource->password,
            'tablename'  => $this->layer->data->tablename,
            'geomcolumn' => $this->layer->data->geomcolumn,
            'sql'        => $this->layer->data->sql,
            'provider'   => $this->datasource->provider,
        ];
        $this->qgislayer->setDatasource($datasource);
        $this->qgis->save();
    }

    public function replaceBy(LayersModel $layer)
    {
        $qgis = new QgisModel($layer->map);
        $sxe = $qgis->getLayerByName($layer->name)->getSimpleXMLElement();
        $this->qgis->addLayer( $sxe );
        $this->qgis->removeLayer( $this->layer->name );
        $this->qgis->updateLayerTree();
        
        $this->layer->name      = $layer->name;
        $this->layer->title     = $layer->title;
        $this->layer->abstract  = $layer->abstract;
        $this->layer->source    = $layer->source;
        $this->layer->save();
        
        $this->layer->data->name           = $layer->data->name;
        $this->layer->data->tablename      = $layer->data->tablename;
        $this->layer->data->displayfield   = $layer->data->displayfield;
        $this->layer->data->geomcolumn     = $layer->data->geomcolumn;
        $this->layer->data->geomtype       = $layer->data->geomtype;
        $this->layer->data->srid           = $layer->data->srid;
        $this->layer->data->extent         = $layer->data->extent;
        $this->layer->data->sql            = $layer->data->sql;
        $this->layer->data->columns        = $layer->data->columns;
        $this->layer->data->datatable      = $layer->data->datatable;
        $this->layer->data->datasource_id  = $layer->data->datasource_id;
        $this->layer->data->save();
        return $this->layer;
    }

    public function moveTo(string $project_name)
    {
        $projectfile = PATH_TO_MAPS . $project_name . '.qgs';
        $qgis = new QgisModel($projectfile);
        $this->layer->map = $projectfile;
        $this->layer->project_name = $project_name;
        $this->layer->save();
        $this->layer->data->project_name = $project_name;
        $this->layer->data->save();
        $qgis->addLayer( $this->qgislayer->getSimpleXMLElement() );
        $qgis->updateLayerTree();
        $this->qgis->removeLayer( $this->layer->name );
        $this->qgis->updateLayerTree();
        return $this->layer;
    }
}