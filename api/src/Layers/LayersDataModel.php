<?php
namespace Sigapp\Layers;

use Illuminate\Database\Eloquent\Model;

class LayersDataModel extends Model
{
    public $incrementing = false;
    protected $table = 'layers_data';
    protected $primaryKey = 'layer_id';
    protected $casts = [
        'layer_id' => 'integer',
        'srid' => 'integer',
        'extent' => 'array',
        'columns' => 'array',
        'opacity' => 'float', 
        'datasource_id' => 'integer'
    ];

	protected $fillable = [
        "layer_id",
        "name", 
        "tablename", 
        "displayfield", 
        "geomcolumn", 
        "geomtype", 
        "srid", 
        "extent", 
        "sql", 
        "columns", 
        "datatable", 
        "opacity", 
        "project_name",
        "datasource_id"
    ];

    public function datasource()
    {
        return $this->hasOne('\Sigapp\Datasources\DatasourcesModel', 'id', 'datasource_id');
    }

    public function layer()
    {
        return $this->hasOne('\Sigapp\Layers\LayersModel', 'id', 'layer_id');
    }    
}