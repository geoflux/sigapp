<?php
namespace Sigapp\Layers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class LayersDataController
{
    public function datatable(Request $request, Response $response, $args)
    {
        $layer = new LayersDataEntity( $args["id"] );
        if ( isset( $args['srid'] ) ) {
            $extent = [
                'xmin' => $args['xmin'],
                'ymin' => $args['ymin'],
                'xmax' => $args['xmax'],
                'ymax' => $args['ymax'],
                'srid' => $args['srid'],
            ];
            $datatable = $layer->getDatatableFromExtent( $extent, $this->getFilter($request) );
        } else {
            $datatable = $layer->getDatatable( $this->getFilter($request) );
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($datatable);
    }

    public function extent(Request $request, Response $response, $args)
    {
        $layer = new LayersDataEntity( $args["id"] );
        if( isset( $args['identifier'] ) ) {
            $extent = $layer->getEntityExtent( $args["identifier"] );
        } else {
            $extent = $layer->getExtent( $this->getFilter($request) );
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($extent);
    }

    public function updateExtent(Request $request, Response $response, $args)
    {
        $layer = new LayersDataEntity( $args["id"] );
        $extent = $layer->getExtent( $this->getFilter($request) );
        $layer = LayersModel::find( $args["id"] );
        $layer->data->extent = $extent;
        $layer->data->save();
        $qgislayer = new LayersQgisUpdater( $layer );
        $qgislayer->updateExtent();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($layer);
    }

    public function columns(Request $request, Response $response, $args)
    {
        $columns = LayersDataModel::find( $args["id"] )->columns;
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($columns);
    }

    public function updateColumns(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $layer = LayersDataModel::find( $args["id"] );
        $layer->columns = $data;
        $layer->save();
        $layerModel = LayersModel::find( $args["id"] );
        $qgislayer = new LayersQgisUpdater($layerModel);
        $qgislayer->updateColumns();
        LayersManager::updateDatatableQuery( $args["id"] );
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($data);
    }

    public function extract(Request $request, Response $response, $args)
    {
        $extent = $request->getParsedBody()['extent'];
        $io = new LayersIO( $args["id"], $args["format"], $this->getFilter($request), $extent );
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson( $io->extract() );
    }

    public function featureinfo(Request $request, Response $response, $args)
    {
        $layer = new LayersDataEntity( $args["id"] );
        if ( isset($args["identifier"]) ) {
            $featureinfo = $layer->getFeatureInfo( $args["identifier"] );
        } else {
            $featureinfo = $layer->getFeatureInfoFromCoordinates(
                $args["x"], 
                $args["y"], 
                $args["zoom"], 
                $this->getFilter($request)
            );
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($featureinfo);
    }

	public function values(Request $request, Response $response, $args)
	{
        $layer = new LayersDataEntity( $args["id"] );
        $values = $layer->getDistinctValues( $args["attribute"] );
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($values);
    }

    public function replace(Request $request, Response $response, $args)
    {
        $layer = LayersModel::find($args['id']);
        $replacement = LayersModel::find($args['layer_id']);
        $qgislayer = new LayersQgisUpdater($layer);
        $result = $qgislayer->replaceBy($replacement);
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($result);
    }

    private function getFilter(Request $request)
    {
        if ( !isset($request->getParams()["filter"]) ) {
            return '';
        }
        return $request->getParams()["filter"];
    }
}