<?php
namespace Sigapp\Layers;

use \Sigapp\Settings\WMS;
use \Sigapp\Layers\Thumbnails\{ Preview, Symbol };

class LayersThumbnails extends WMS
{
    public function __construct($layer_id)
    {
        $this->layer = LayersModel::find($layer_id);
        parent::__construct($this->layer->name, $this->layer->project_name);
    }

    public function create()
    {
        return [
            'preview_url' => ( new Preview($this->layer->id) )->create(),
            'symbol_url' => ( new Symbol($this->layer->id) )->create(),
        ];
    }
}