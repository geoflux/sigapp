<?php
namespace Sigapp\Layers;

use \Sigapp\Layers\IO\GeoJson;

class LayersDataEntity extends AbstractLayers implements LayersDataEntityInterface
{
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
        $this->getLayer();
    }

	public function getDatatable($filter = '')
	{
		return $this->getProvider()->getDatatable($filter);
	}
	
	public function getDatatableFromExtent(array $extent, $filter = '')
	{
		return $this->getProvider()->getDatatableFromExtent($extent, $filter);
    }
    
	public function getExtent($filter = '')
	{
		return $this->getProvider()->getExtent($filter);
    }
    
	public function getFeatureInfo($identifier)
	{
		$data = $this->getProvider()->getFeatureInfo($identifier);
		return ( new GeoJson($data) )->toGeoJson();
    }
    
	public function getFeatureInfoFromCoordinates($x, $y, $zoom, $filter = '')
	{
		$data = $this->getProvider()->getFeatureInfoFromCoordinates($x, $y, $zoom, $filter);
		if ($data) {
			return ( new GeoJson($data) )->toGeoJson();
		}
		return false;
	}

	public function getEntityExtent($identifier)
	{
		return $this->getProvider()->getEntityExtent($identifier);
    }

	public function getDistinctValues($attribute)
	{
		return $this->getProvider()->getDistinctValues($attribute);
	}
}