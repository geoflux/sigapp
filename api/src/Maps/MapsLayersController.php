<?php
namespace Sigapp\Maps;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class MapsLayersController
{
    public function index(RequestInterface $request, ResponseInterface $response, $args)
    {

    }

    public function store(RequestInterface $request, ResponseInterface $response, $args)
    {

    }

    public function update(Request $request, Response $response, $args)
    {

    }

    public function reorder(Request $request, Response $response, $args)
    {

    }

    public function preview(Request $request, Response $response, $args)
    {

    }

    public function destroy(Request $request, Response $response, $args)
    {

    }
}