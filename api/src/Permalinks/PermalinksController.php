<?php
namespace Sigapp\Permalinks;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class PermalinksController
{
    public function store(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $permalink = [
            'token' => $args['token'],
            'map' => $data,
            'type' =>  isset( $data['type'] ) ? $data['type'] : 'link',
            'user_id' => USER
        ];
        PermalinksModel::create($permalink);
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($permalink);
    }

    public function show(Request $request, Response $response, $args)
    {
        $permalink = PermalinksModel::where( 'token', $args['token'] )->first();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson( $permalink->map );
    }

    public function destroy(Request $request, Response $response)
    {
        PermalinksModel::where( 'type', 'link' )->destroy();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson( true );
    }

    public function export(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $printer = new PermalinksPrinter($data['url'], $data['jwt']);
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson( $printer->capture() );
    }
}