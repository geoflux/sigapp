<?php
namespace Sigapp\Datasources;

use \Sigapp\Layers\LayersDataModel;
use \Spatialite\SPL;
use Symfony\Component\Filesystem\Filesystem;

class DatasourcesManager
{
    public function __construct(array $datasource)
    {
        $this->datasource = $datasource;
    }

    public function create()
    {
        $datasource = DatasourcesModel::create($this->datasource);
        return $datasource->id;
    }

    public function createIfNotRecorded()
    {
        $isRecorded = DatasourcesModel::where([
            ['provider', '=', $this->datasource["provider"]],
            ['dbname', '=', $this->datasource["dbname"]]
        ])->first();
        if ($isRecorded === null){
            return $this->create();
        }
        return $isRecorded->id;
    }

    public function dumpLocalData(array $data)
    {
        $db = new SPL( PATH_TO_MAPS . $this->datasource['dbname'] );
        if ( in_array($data['extension'], ['shp', 'shx', 'dbf', 'cpg', 'prj', 'qpj']) ) {
            return $db->loadShapefile( $data['file'], $data['tablename'], [
                'srid'          => $data['srid'],
                'displayfield'  => $data['displayfield'],
                // 'charset'       => ($data['encoding'] === 'System') ? 'CP1252' : $data['encoding']
            ]);
        }
        return false;
    }

    public static function createLocalStorage($datasource)
    {
        if ( $datasource['provider'] === 'spatialite') {
            $localStorage = PATH_TO_MAPS . $datasource['dbname'];
            SPL::CreateNewEmptyDb( $localStorage );
        }
    }

    public static function destroy() {
        $datasources = DatasourcesModel::whereNotIn('id', LayersDataModel::pluck('datasource_id'))->get();
        foreach ($datasources as $datasource) {
            if($datasource->provider === 'spatialite') {
                ( new Filesystem )->remove( realpath(PATH_TO_MAPS . $datasource->dbname) );     
            }
            DatasourcesModel::where('id', $datasource->id)->delete();
        }
        return $datasources;
    }
}