<?php
namespace Sigapp\Datasources;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class DatasourcesController
{
    public function index(Request $request, Response $response)
    {
        $datasources = DatasourcesModel::orderBy('created_at')->get();
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withJson($datasources);
    }

    public function destroy(Request $request, Response $response, $args)
    {
        $removed = DatasourcesManager::destroy();
        return $response
            ->withHeader('Content-Type', 'application/json')
            // ->withJson( $removed );
            ->withJson( DatasourcesModel::orderBy('created_at')->get() );
    }    
}